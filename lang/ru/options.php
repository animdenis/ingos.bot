<?
//errors
$MESS['MODULE_IBLOCK_ERROR'] = '�� ���������� ������ iblock';
$MESS['MODULE_CATALOG_ERROR'] = '�� ���������� ������ catalog';
$MESS['MODULE_SALE_ERROR'] = '�� ���������� ������ sale';

$MESS['TOKEN_IS_EMPTY'] = '����� �� ����������';
$MESS['TOKEN_IS_SET'] = '����� ���������� (#URL#)';

$MESS['COUNT_ZERO_IBLOCKS'] = '��� ���������� ��� ��������';
$MESS['COUNT_ZERO_ORDER_PROPS'] = '����������� �������� ��� ���������� ������';
$MESS['COUNT_ZERO_USER_GROUPS'] = '����������� ������ ��� ����������� �������������';
$MESS['COUNT_ZERO_SITE'] = '����������� ������ ������';
$MESS['COUNT_ZERO_PERSON_TYPES'] = '����������� ���� ������������';
$MESS['COUNT_ZERO_DELIVERY'] = '����������� ������� ��������';
$MESS['CHOOSE_IBLOCK'] = '�������� ��������';
$MESS['COUNT_ZERO_CURRS'] = '������ �����������';
//tabs
$MESS['IT_TAB_MAIN'] = '����� ���������';
$MESS['IT_TAB_CATALOG'] = '��������� ����������� ��������';
$MESS['NOT'] = '�� �������';
//headings
$MESS['IT_HEAD_BOT'] = '��������� ����';
$MESS['IT_HEAD_CATALOG'] = '�������� ��������� ��������';
$MESS['IT_HEAD_ORDER'] = '���������� ������';
$MESS['IT_BOTHELLO'] = '��������� � ������� "������"';
$MESS['IT_CATALOGCURRENCY'] = '�������� ������:';


//params
$MESS['IT_BOTNAME'] = '��� ����:';
$MESS['IT_BOTSITEPATH'] = '������ ������ �����:';
$MESS['IT_BOTREDIRECT'] = '�������� ��� ������ ������:';
$MESS['IT_TOKEN'] = 'Token:';
$MESS['IT_WEBHOOKURL'] = '���� �� �������:';
	$MESS['IT_WEBHOOKURL_SUP'] = '(��-��������� ����� ������ ����)';
$MESS['IT_MAXUSERS'] = '������������ ����� HTTPS ����������:';
$MESS['IT_USERGROUP'] = '����������� ������������� � ������:';

$MESS['IT_CATALOGIBLOCK'] = '�������� ��������:';
$MESS['IT_CATALOGIPRICE'] = '���� ���:';

$MESS['SECTION_LIST'] = '��������� ��������:';
$MESS['IT_CATALOGSORTSECTION'] = '���� ���������� ��������:';
$MESS['IT_CATALOGBYSECTION'] = '����������� ���������� ��������:';
$MESS['IT_CATALOGPAGECOUNTSECTION'] = '���������� �������� �� ��������:';
$MESS['IT_CATALOGTWICESECTION'] = '���������� ������� � 2 ����:';
$MESS['IT_CATALOGCOUNTSECTION'] = '���������� ���������� ��������� �������:';
	//sort
	$MESS['SORTID'] = '[SORT] ���� ����������';
	$MESS['SORTSORT'] = '[ID] �������������';
	$MESS['ASC'] = '�� �����������';
	$MESS['DESC'] = '�� ��������';

$MESS['SECTION'] = '��������� ������ �������:';
$MESS['IT_CATALOGSORTELEMENT'] = '���� ���������� �������:';
$MESS['IT_CATALOGBYELEMENT'] = '����������� ���������� �������:';
$MESS['IT_CATALOGPAGECOUNTELEMENT'] = '���������� ��������� �� ��������:';
$MESS['IT_CATALOGHIDENOTAVAILELEMENT'] = '�������� ����������� � ������� ������:';
$MESS['IT_CATALOGSHOWONLYGOODS'] = '���������� �� ������ ������ �������� ������ ������:';

$MESS['ELEMENT'] = '��������� ��������� ������:';
$MESS['IT_CATALOGELEMENTDESCR'] = '���� �������� ������:';
	$MESS['PREVIEW_TEXT'] = '�����';
	$MESS['DETAIL_TEXT'] = '��������';
$MESS['IT_CATALOGELEMENTPHOTOFIELD'] = '���� �������� ������:';
$MESS['IT_CATALOGELEMENTPHOTOPROP'] = '�������� �������� ������ (������������ ���� ��������):';
	$MESS['PREVIEW_PICTURE'] = '����� -> ���������';
	$MESS['DETAIL_PICTURE'] = '��������� -> �����';
$MESS['IT_CATALOGELEMENTHTMLSPLIT'] = '�������� HTML-���� � ��������:';
$MESS['IT_CATALOGELEMENTPROPS'] = '��������� �������� ������:';

$MESS['IT_ORDERPTYPE'] = '��� �����������:';
$MESS['IT_ORDERPROPS'] = '��������� �������� ������:';
$MESS['IT_ORDERPROPSPHONE'] = '�������� ��� ������ ��������� ������� (�������):';
$MESS['IT_ORDERPROPSLOC'] = '�������� ��� ������ �������������� �������:';
$MESS['IT_ORDERPROPSADDRESS'] = '�������� ��� ������ ������ �������:';
$MESS['IT_ORDERDELIVERY'] = '������� ��������:';
$MESS['IT_ORDERPAYSYSTEM'] = '������� ������:';
$MESS['IT_ORDERCOMPONENT'] = '���� �� ���������� sale.order.ajax:';