<?
$dbPersonType = CSalePersonType::GetList(["SORT" => "ASC"], ["LID"=>$defSite]);
$personType = [];
$personTypeId = $_REQUEST['bot_order_person_type']?:COption::GetOptionString($mid, 'bot_order_person_type');
while($obPersonType = $dbPersonType->Fetch())
{
	if($personTypeId == NULL)
	{
		COption::SetOptionString($mid, 'bot_order_person_type', $obPersonType['ID']);
		$personTypeId = $obPersonType['ID'];
	}

    $personType[$obPersonType['ID']] = $obPersonType['NAME'];
}

if(count($personType) == 0) $errorList[] = GetMessage('COUNT_ZERO_PERSON_TYPES');

$dbOrdProps = CSaleOrderProps::GetList(["SORT" => "asc"], ["PERSON_TYPE_ID" => $personTypeId, 'LID' => $defSite, "UTIL" => "N"], false, false, []);

$phoneProps['-'] = GetMessage('NOT');
$locProps['-'] = GetMessage('NOT');
while($obOrdProp = $dbOrdProps->Fetch())
{
    $ordProps[$obOrdProp['ID']] = '[' . $obOrdProp['CODE'] . '] ' .$obOrdProp['NAME'];
	
	if($obOrdProp['TYPE'] == 'TEXT' || $obOrdProp['TYPE'] == 'TEXTAREA')
		$phoneProps[$obOrdProp['CODE']] = '[' . $obOrdProp['CODE'] . '] ' .$obOrdProp['NAME'];
	if($obOrdProp['IS_LOCATION'] == 'Y')
		$locProps[$obOrdProp['CODE']] = '[' . $obOrdProp['CODE'] . '] ' .$obOrdProp['NAME'];

}

if(count($ordProps) == 0) $errorList[] = GetMessage('COUNT_ZERO_ORDER_PROPS');