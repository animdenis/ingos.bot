<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

die(); //TO DO -- REPLY FOR EDIT ORDER FIELDS

$user = new \Itandyr\Salebot\User($result['message']['from']);
$order = new \Itandyr\Salebot\Order();
$msg = new \Itandyr\Salebot\Msg();

$orderFields = $order->getOrderFields();

if(SITE_CHARSET == 'windows-1251')
{
	$orderFields = \Bitrix\Main\Text\Encoding::convertEncoding($orderFields, 'windows-1251', 'UTF-8');
	$titlePhone = \Bitrix\Main\Text\Encoding::convertEncoding($botOptions->getTitle('GET_PHONE'), 'windows-1251', 'UTF-8');
}

if($result['message']['contact']['phone_number'] !== NULL)
{
	$replyMessage = $orderFields[$botOptions->getOption('order', 'bot_order_props_phone')];
	$value = $result['message']['contact']['phone_number'];
}
else
{
	$replyMessage = $result['message']['reply_to_message']['text'];
	$value = $result['message']['text'];
}

$order->setOrderField($replyMessage, $value);

$step = 1;

foreach($orderFields as $code => $orderField)
{
	if($orderField == $replyMessage)
	{		
		$msg = $order->getOrderStep($step);
		break;
	}

	$step++;
}

if($orderFields[$botOptions->getOption('order', 'bot_order_props_phone')] === $msg->getText())
	$telegram->sendMessage([ 
		'chat_id' => $result["message"]["chat"]["id"],
		'text' => $msg->getText(),
		'reply_markup' => $telegram->replyKeyboardMarkup(['keyboard' => [[['text' => $titlePhone, 'request_contact' => true]]]])
	]);
if('Город' === $msg->getText())
	$telegram->sendMessage([ 
		'chat_id' => $result["message"]["chat"]["id"],
		'text' => $msg->getText(),
		'reply_markup' => $telegram->replyKeyboardMarkup(['keyboard' => [[['text' => $titlePhone, 'request_location' => true]]]])
	]);
else if($msg->getText())
	$telegram->sendMessage([ 
		'chat_id' => $result["message"]["chat"]["id"],
		'text' => $msg->getText(),
		'reply_markup' => $telegram->replyKeyboardMarkup(['force_reply' => true])
	]);
else
{
	//$msg = $order->checkOrderFields();
	$msg = $order->getDelivery();
	$telegram->sendMessage([ 
		'chat_id' => $result["message"]["chat"]["id"],
		'text' => $msg->getText(),
		'reply_markup' => $telegram->replyKeyboardMarkup(['inline_keyboard' => $msg->getKeyboard()])
	]);
}