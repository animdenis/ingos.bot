<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$user = new \Ingos\Bot\User($result['callback_query']->getFrom());

$data = \Ingos\Bot\Utils::callBackToArray($result['callback_query']['data']);
$msg = new \Ingos\Bot\Msg();

$refreshKeyboard = false;
//AddMessage2Log($result['callback_query']['message']['text']);
$chatId = $result['callback_query']['message']['chat']['id'];
$msgId = $result['callback_query']['message']['message_id'];

switch ($data['case'])
{
	/*CATALOG*/
	
		case ($botOptions->getCase('sectionList')):
			$catalog = new \Ingos\Bot\Catalog();
			$msg = $catalog->getSection($data['id'], $data['page']);
		break;
		
		case ($botOptions->getCase('elementList')):
			$catalog = new \Ingos\Bot\Catalog();
			$msg = $catalog->getElementList($data['id'], $data['page']);
		break;
		
		case ($botOptions->getCase('elementPage')):
			$catalog = new \Ingos\Bot\Catalog();
			$msg = $catalog->getElement($data['id'], [
				'nElNum' => $data['nElNum'],
				'sumEl' => $data['sumEl']
			]);
		break;
		
		case ($botOptions->getCase('calcProcess')):
			$calcClass = "\Ingos\Bot\Calc\\".$data['class'];
			$isSetField = true;
			
			if(!isset($data['value']))
				\Ingos\Bot\Utils::setUserField('UF_CALC_CLASS', $data['class']);
			else
				$isSetField = $calcClass::setField($data['value']);
			
			if(!$isSetField)
			{
				$messageParams = [ 
					'chat_id' => $chatId,
					'text' => 'Ошибка ввода, повторите заново',
					'parse_mode' => 'HTML'
				];
				$telegram->sendMessage($messageParams);
			}
			
			$msg = $calcClass::getField($data['start'] == 1);
			
			if(!$msg->getKeyboard())
			{
				$messageParams = [ 
					'chat_id' => $chatId,
					'text' => $msg->getText(),
					'parse_mode' => 'HTML'
				];
				$telegram->sendMessage($messageParams);
				exit;
			}
			elseif($msg->getKeyboard() == 'location')
			{
				$messageParams = [ 
					'chat_id' => $chatId,
					'text' => $msg->getText(),
					'parse_mode' => 'HTML',
					'reply_markup' => $telegram->replyKeyboardMarkup(['keyboard' => [[['text' => 'Поделиться местоположением', 'request_location' => true]]]])
				];
				$telegram->sendMessage($messageParams);
				exit;
			}
			elseif($msg->getKeyboard() == 'contacts')
			{
				$messageParams = [ 
					'chat_id' => $chatId,
					'text' => $msg->getText(),
					'parse_mode' => 'HTML',
					'reply_markup' => $telegram->replyKeyboardMarkup(['keyboard' => [[['text' => 'Поделиться контактами', 'request_contact' => true]]]])
				];
				$telegram->sendMessage($messageParams);
				exit;
			}
		break;
		
		case($botOptions->getCase('calcResult')):
			$calcClass = "\Ingos\Bot\Calc\\".$data['class'];
			\Ingos\Bot\Utils::setUserField('UF_CALC_CLASS', '');
			$msg = $calcClass::setResult($data['value']);
			$refreshKeyboard = true;
		break;
		
}

$text = $msg->getText();
$keyboard = $msg->getKeyboard();

if($text)
	$telegram->editMessageText([
		'chat_id' => $chatId,
		'message_id' => $msgId,
		'text' => $text,
		'parse_mode' => 'HTML'
	]);
if($keyboard)
	$telegram->editMessageReplyMarkup([
		'chat_id' => $chatId,
		'message_id' => $msgId,
		'reply_markup' => $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard])
	]);
	
if($refreshKeyboard)
{
	$msg = new \Ingos\Bot\Msg($botOptions->getTitle('GET_START_MSG'));	
	$telegram->sendMessage([ 
		'chat_id' => $chatId,
		'text' => $msg->getText(),
		'reply_markup' => $telegram->replyKeyboardMarkup(['keyboard' => $botOptions->getKeyboard(),'resize_keyboard' => true,'one_time_keyboard' => false]),
		'parse_mode' => 'HTML',
		'disable_web_page_preview' => false 
	]);
}