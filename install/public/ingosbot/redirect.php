<?
if(empty($_GET['UID']) || empty($_GET['AUTH']) || empty($_GET['REDIRECT'])) die('Please, stop!');

require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
global $USER;
$rsUser = CUser::GetByLogin($_GET['UID']);
if($obUser = $rsUser->GetNext())
{
	//print_r($obUser);
	if($obUser['PASSWORD'] === $_GET['AUTH'])
	{
		$USER->Authorize($obUser['ID'], 1);
		LocalRedirect($_GET['REDIRECT']);
	}
}
else
{
	die('Auth error!');
}

//else
	//LocalRedirect($_GET['REDIRECT']);