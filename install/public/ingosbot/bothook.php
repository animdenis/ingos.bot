<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$user = new \Ingos\Bot\User($result['message']['from'], $result["message"]["chat"]["id"]);
$msg = new \Ingos\Bot\Msg($botOptions->getTitle('GET_START_MSG'));

switch($result["message"]["text"])
{
	case ($botOptions->getAction('getCatalog')):
        $catalog = new \Ingos\Bot\Catalog();
        $msg = $catalog->getSection(false, 1);
	break;
	
	default:
		
		if($class = $user->getCalcClass())
		{
			$calcClass = '\Ingos\Bot\Calc\\' . $class;
			$isSetField = true;
			if($result['message']['contact']['phone_number'] !== NULL)
				$calcClass::setField($result['message']['contact']['phone_number']);
			
			if($result['message']['location'] !== NULL)
				$calcClass::setLocation(['lat' => $result['message']['location']['latitude'], 'lon' => $result['message']['location']['longitude']]);
			else
			{
				if(SITE_CHARSET == 'windows-1251')
					$result["message"]["text"] = \Bitrix\Main\Text\Encoding::convertEncoding($result["message"]["text"], 'UTF-8', 'windows-1251');
				
				$isSetField = $calcClass::setField($result["message"]["text"]);
				
				if(!$isSetField)
				{
					$messageParams = [ 
						'chat_id' => $result["message"]["chat"]["id"],
						'text' => 'Ошибка ввода, повторите заново',
						'parse_mode' => 'HTML'
					];
					$telegram->sendMessage($messageParams);
				}
			}
			
			$msg = $calcClass::getField();
			
			$messageParams = [ 
				'chat_id' => $result["message"]["chat"]["id"],
				'text' => $msg->getText(),
				'parse_mode' => 'HTML'
			];
			
			if($keyboard = $msg->getKeyboard())
				$messageParams['reply_markup'] = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
		
			$telegram->sendMessage($messageParams);
			exit;
		}
	
		
	
}

$text = $msg->getText();
$keyboard = $msg->getKeyboard() ? $telegram->replyKeyboardMarkup(['inline_keyboard' => $msg->getKeyboard()]) : $telegram->replyKeyboardMarkup(['keyboard' => $botOptions->getKeyboard(),'resize_keyboard' => true,'one_time_keyboard' => false]);

if(!empty($text))
	$telegram->sendMessage([ 
		'chat_id' => $result["message"]["chat"]["id"],
		'text' => $text,
		'reply_markup' => $keyboard,
		'parse_mode' => 'HTML',
		'disable_web_page_preview' => false 
	]);