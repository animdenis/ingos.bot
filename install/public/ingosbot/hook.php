<?php
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
use Bitrix\Main\Loader;
if(!Loader::includeModule("ingos.bot")) die();

	global $botOptions;
	$botOptions = new \Ingos\Bot\Options();

	$token = $botOptions->getOption('main', 'bot_token');
	echo $token;
if(!$token) die();
if($token !== $_GET['token']) die();



	//echo $botOptions->getApiPath();die();

	require $botOptions->getApiPath();

	use Telegram\Bot\Api; 
	use Telegram\Bot\Keyboard\Keyboard;
	use Telegram\Bot\Actions;
	use Telegram\Bot\Commands\Command;
	use Telegram\Bot\Objects\CallbackQuery;
	
	$telegram = new Api($token);

	$result = $telegram->getWebhookUpdates();

	global $globalElementList;
	require __DIR__ . '/filter.php';

	require __DIR__ . '/typing.php';

	if ($result->isType('callback_query'))
		require __DIR__ . '/callback.php';
	else
		require __DIR__ . '/bothook.php';
	/*elseif($result['message']['reply_to_message'] !== NULL)
		require __DIR__ . '/reply.php';*/
	