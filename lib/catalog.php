<?
namespace Ingos\Bot;

use Bitrix\Main\Loader;

Loader::includeModule("iblock");

class Catalog
{
    private $opt = false;

    public function __construct()
    {
        global $botOptions;
        $this->opt = $botOptions;
    }
	
	/*PROTECTED FUNCTIONS*/
	
        protected function _getSectionField($sectionId = false, $field = false)
        {
            if(!$sectionId||!$field) return false;

            $arFilter = [
                'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
                'ID' => $sectionId,
                'ACTIVE' => 'Y'
            ];

            $dbSection = \CIblockSection::GetList(array(), $arFilter, false, array($field), false);
            if($obSection = $dbSection->GetNext())
                return $obSection[$field] ? $obSection[$field] : false;
            else
                return false;
        }

		protected function _getSectionPath($sectionId = false)
		{
			if((int)$sectionId == 0)
				return $this->opt->getTitle('CATALOG_TITLE');

			$depth = $this->_getSectionField($sectionId, 'DEPTH_LEVEL');
			$arTree = [];
			for($i = 1; $i <= $depth; $i++)
			{
				$arTree[] = $this->_getSectionField($sectionId, 'NAME');
				$sectionId = $this->_getSectionField($sectionId, 'IBLOCK_SECTION_ID');
			}
			$arTree[] = $this->opt->getTitle('CATALOG_TITLE');
			$arTree = array_reverse($arTree);

			return is_array($arTree) ? implode($this->opt->getTitle('BACK_SEPARATOR'), $arTree) : false;
		}

		protected function _getBackSectionButton($sectionId = false, $getParenSection = true, $parentPage = 1)
		{
			if($getParenSection)
				$sectionId = $this->_getSectionField($sectionId, 'IBLOCK_SECTION_ID');
			return [[[
				'text' => $this->opt->getTitle('BACK_BUTTON'),
				'callback_data' => Utils::getCallBackStr([
					'case' => $this->opt->getCase('sectionList'),
					'id' => $sectionId,
					'page' => $parentPage?:1
				])
			]]];
		}
	
		protected function _getElementMenu($elementId = false, $sectionId = false, $arNav = [])
		{
			$arElMenu = [
				[[
					'text' => $this->opt->getTitle('ADD_TO_CART'),
					'callback_data' => Utils::getCallBackStr([
						'case' => $this->opt->getCase('calcProcess'),
						'class' => 'Osago',
						'start' => '1'
					])
				]],
				[[
					'text' => $this->opt->getTitle('FAQ'),
					'url' => 'http://telegra.ph/Zelenaya-karta-12-09'
				]]
				
			];
			
			if($sectionId && is_array($arNav))
			{
				$arNav['IDs'] = $this->_getElementNavIDs($elementId, $sectionId);
				$arElMenu = array_merge($arElMenu, CatalogUtils::getElementNav($this->opt->getCase('elementPage'), $elementId, $arNav));
			}
			
			return $arElMenu; 
		}
		
		protected function _getElementNavIDs($elementId = false, $sectionId = false)
		{
			$sort = CatalogUtils::getSortArray(); 
			$arFilter = [
				'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
				'SECTION_ID' => $sectionId,
				'ACTIVE' => 'Y'
			];
			$arNavParams = [
				'nElementID' => $elementId,
				'nPageSize' => 1
			];
			$arSelect = ['ID'];
			$dbElement = \CIblockElement::GetList($sort, $arFilter, false, $arNavParams, $arSelect);
			$navIds = [];
			while($obElement = $dbElement->GetNext())
				$navIds[] = $obElement['ID'];
			
			return is_array($navIds) ? $navIds : false;
		}
	
	/**/

    //public catalog functions
    public function getSection($sectionId = false, $currentPage = 1)
    {
		if($this->opt->getOption('catalog', 'bot_catalog_show_only_goods') == 'Y')
			return $this->getElementList(false, 1, 1);

        $pageCount = $this->opt->getOption('catalog', 'bot_catalog_pagecount_section');
        $sort = CatalogUtils::getSortArray('SECTION_LIST'); 
        $arFilter = [
            'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
            'ACTIVE' => 'Y',
            'CNT_ACTIVE' => 'Y'
        ];
        $cnt = $this->opt->getOption('catalog', 'bot_catalog_count_section') == 'Y' ? true : false;
        $arFields = ['ID', 'NAME'];

        if(!$sectionId)
            $arFilter['DEPTH_LEVEL'] = 1;
        else
            $arFilter['SECTION_ID'] = $sectionId;

        $arNavStartParams = [
            'iNumPage' => $currentPage,
            'nPageSize' => $pageCount
        ];

        $dbSection = \CIblockSection::GetList($sort, $arFilter, true, $arFields, $arNavStartParams);
        $sectionMenu = [];
        
        while($obSection = $dbSection->GetNext())
        {
            //if($obSection['ELEMENT_CNT'] == 0) continue;

            if($cnt) $obSection['NAME'] .= ' (' . $obSection['ELEMENT_CNT'] . ')';
            $sectionMenu[] = [
                'text' => $obSection['NAME'],
                'callback_data' => Utils::getCallBackStr([
                    'case' => $this->opt->getCase('sectionList'),
                    'id' => $obSection['ID'],
                    'page' => 1
                ])
            ];
        }

        $chunk = $this->opt->getOption('catalog', 'bot_catalog_twice_section') == 'Y' ? 2 : 1;
        $sectionMenu = array_chunk($sectionMenu, $chunk);
        
        /*paginationQuery*/
        $dbSumSection = \CIblockSection::GetList($sort, $arFilter, false, [], false);
        $sumElements = $dbSumSection->SelectedRowsCount();       
        if($sumElements > $pageCount)
        {
            $sumPages = (int)ceil($sumElements / $pageCount);            
            $sectionMenu = array_merge($sectionMenu, CatalogUtils::getPageNav($this->opt->getCase('sectionList'), $sectionId, $currentPage, $sumPages));
        }

        if(empty($sectionMenu) && $sectionId)
            return $this->getElementList($sectionId, 1, $currentPage);

        //AddMessage2Log($sectionMenu);
        if($sectionId)
            $sectionMenu = array_merge($sectionMenu, $this->_getBackSectionButton($sectionId));

		$text =  $this->_getSectionPath($sectionId);
        return new Msg($text, $sectionMenu);
    }
		

    public function getElementList($sectionId = false, $startNavPage = 1)
    {
        $pageCount = $this->opt->getOption('catalog', 'bot_catalog_pagecount_element');
        $sort = CatalogUtils::getSortArray(); 
        $arFilter = [
            'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
            'ACTIVE' => 'Y'
        ];
		
		if($sectionId)
			$arFilter['SECTION_ID'] = $sectionId;
		
		global $globalElementList; //include in public (/salebot/filter.php)
		if(!empty($globalElementList))
			$arFilter = array_merge($arFilter, $globalElementList);
		
        /*if($this->opt->getOption('catalog', 'bot_catalog_hidenotavail_element') == 'Y')
      		$arFilter['CATALOG_AVAILABLE'] = 'Y';
        $arFilter['CATALOG_TYPE'] = 1;*/

        $arNavStart = [
            'nPageSize' => $pageCount,
            'iNumPage' => $startNavPage
        ];
        $arSelect = ['NAME', 'ID'];

        /*paginationQuery*/
        $dbSumElement = \CIblockElement::GetList($sort, $arFilter, false, false, $arSelect);
        $sumElements = $dbSumElement->SelectedRowsCount();
        $cnt = ($startNavPage - 1) * $pageCount + 1;

        $dbElement = \CIblockElement::GetList($sort, $arFilter, false, $arNavStart, $arSelect);
        $elementListMenu = [];
        while($obElement = $dbElement->GetNext())
        {
            $price = CatalogUtils::getPrice($obElement['ID']);

            $elementListMenu[] = [
                'text' => $obElement['NAME'],
                'callback_data' => Utils::getCallBackStr([
                    'case' => $this->opt->getCase('elementPage'),
                    'id' => $obElement['ID'],
                    'nElNum' => $cnt,
                    'sumEl' => $sumElements
                ])
            ];

            $cnt++;
        }

        $elementListMenu = array_chunk($elementListMenu, 1);

        if($sumElements > $pageCount)
        {
            $sumPages = floor($sumElements / $pageCount);
            if(floor($sumElements / $pageCount) != ($sumElements / $pageCount))
                $sumPages++;
            $elementListMenu = array_merge($elementListMenu, CatalogUtils::getPageNav($this->opt->getCase('elementList'), $sectionId, $startNavPage, $sumPages));
        }

        if($sectionId)
            $elementListMenu = array_merge($elementListMenu, $this->_getBackSectionButton($sectionId));

		$text =  $this->_getSectionPath($sectionId);
        return new Msg($text, $elementListMenu);
    }

    public function getElement($elementId = false, $arNav = [])
    {
		//check minimal
		$iblockId = $this->opt->getOption('catalog', 'bot_catalog_iblock');
		if($iblockId === NULL || $iblockId < 0 || $elementId < 0 || empty($elementId)) return false; /*TODO create message with text*/
		//get list params
		$sort = CatalogUtils::getSortArray();
        $arFilter = [
            'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
            'ID' => $elementId,
            'ACTIVE' => 'Y'
        ];
        $arSelect = ['NAME', 'ID', 'DETAIL_PICTURE', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID', 'DETAIL_TEXT', 'PREVIEW_TEXT'];
		//properties
        $arPropCode = explode(',', $this->opt->getOption('catalog', 'bot_catalog_element_props'));
        $arSelectProps = [];
        if(count($arPropCode) > 0)
            foreach ($arPropCode as &$value)
            {
                $value = explode('|', $value);
                $value = ['CODE' => $value[0], 'NAME' => $value[1]];
                $arSelectProps[] = 'PROPERTY_' . $value['CODE'];
            }
		//photo props
        $photoProp = $this->opt->getOption('catalog', 'bot_catalog_element_photoprop');
        if($photoProp !== 'NOT' && $photoProp !== NULL)
            $photoProp = ['PROPERTY_' . $photoProp];
        else
            $photoProp = [];
		//finall select param
        $arSelect = array_merge($arSelect, $arSelectProps, $photoProp);

        $dbElement = \CIblockElement::GetList($sort, $arFilter, false, false, $arSelect);
        $elMenu = [];
        if($obElement = $dbElement->GetNext())
        {
			$arElementText = [
				'name' => CatalogUtils::getElementName($obElement),
				'descr' => CatalogUtils::getElementDescr($obElement),
				'props' => (count($arPropCode) > 0) ? CatalogUtils::getElementProps($obElement, $arPropCode) : ''
			];
			
			if($arElementText['descr'] == '' || empty($arElementText['descr'])) unset($arElementText['descr']);
			if($arElementText['props'] == '' || empty($arElementText['props'])) unset($arElementText['props']);

            $text = implode($this->opt->getTitle('GET_DELIMITER_MSG'), $arElementText);
			
            $keyboard = array_merge(
                $this->_getElementMenu($elementId, $obElement['IBLOCK_SECTION_ID'], $arNav),
                $this->_getBackSectionButton($obElement['IBLOCK_SECTION_ID'], false)
            );
			
			return new Msg($text, $keyboard);
        }
		else
		{
			/*TODO create message with text*/
			return false;
		}
    }
}