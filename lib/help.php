<?
namespace Itandyr\Salebot;

class Help
{	
	public function getHelpMessage()
	{
		global $botOptions;
		return new Msg($botOptions->getOption('main', 'bot_help'));
	}
}