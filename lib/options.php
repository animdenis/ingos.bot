<?
namespace Ingos\Bot;

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

class Options
{
    const MID = 'ingos.bot';

    private $options = array (
      'main' => 
              array (
                'bot_siteurl' => 'https://demo.itandyr.pro',
                'bot_token' => '434403815:AAFggcqTK-YORQE2J_Mf1T_AZv-3OS5_4sM',
                'bot_user_group' => '3',
                'bot_site_id' => 's1',
                'bot_help' => '',
				'bot_redirect' => ''
              ),
      'catalog' => 
              array (
                'bot_catalog_iblock' => '5',
				'bot_catalog_currency' => 'RUB',
                'bot_catalog_iblock_price' => 'BASE',
                'bot_catalog_sort_section' => 'ID',
                'bot_catalog_by_section' => 'ASC',
                'bot_catalog_pagecount_section' => '9',
                'bot_catalog_twice_section' => 'N',
                'bot_catalog_count_section' => 'N',
                'bot_catalog_sort_element' => 'ID',
                'bot_catalog_by_element' => 'ASC',
                'bot_catalog_pagecount_element' => '9',
                'bot_catalog_hidenotavail_element' => 'N',
				'bot_catalog_show_only_goods' => 'N',
                'bot_catalog_element_descr' => 'DETAIL_TEXT',
                'bot_catalog_element_htmlsplit' => 'Y',
                'bot_catalog_element_photofields' => 'DETAIL_PICTURE',
                'bot_catalog_element_photoprop' => 'MORE_PHOTO',
                'bot_catalog_element_props' => '',
              ),
        'order' => array(
                'bot_order_person_type' => 1,
                'bot_order_props' => '',
                'bot_order_props_phone' => '',
				'bot_order_props_loc' => '',
				'bot_order_props_address' => '',
                'bot_order_delivery' => '',
                'bot_order_paysystem' => '',
                'bot_order_component' => '/personal/order/'
            ),
    );
    private $actionBoard = false;
    private $cases = [
        'sectionList' => 'sectionList',
        'elementList' => 'elementList',
        'elementPage' => 'elementPage',
        'addBasket' => 'addBasket',
        'delBasket' => 'delBasket',
        'clearBasket' => 'clearBasket',
        'changeQty' => 'changeQty',
        'getBasketItem' => 'getBasketItem',
        'createOrder' => 'createOrder',
        'getOrderStatusList' => 'getOrderStatusList',
        'getOrderList' => 'getOrderList',
        'getOrder' => 'getOrder',
		'calcProcess' => 'calcProcess',
		'calcResult' => 'calcResult'
    ];
    private $titles = [];

    public function __construct()
    {
        $this->actionBoard = [
            'getCatalog' => GetMessage('GET_CATALOG'),
            'getServices' => GetMessage('GET_SERVICES'),
            'getStrakh' => GetMessage('GET_STRAKH'),
            'getFaq' => GetMessage('GET_FAQ'),
            'getHelp' => GetMessage('GET_HELP')
        ];

        $this->titles = [
            'CATALOG_TITLE' => GetMessage("CATALOG_TITLE"),
            'BACK_SEPARATOR' => GetMessage("BACK_SEPARATOR"),
            'BACK_BUTTON' => GetMessage("BACK_BUTTON"),
            'ADD_TO_CART' => GetMessage("ADD_TO_CART"),
            'NEXT_PAGE' => GetMessage("NEXT_PAGE"),
            'PREV_PAGE' => GetMessage("PREV_PAGE"),
			'GET_PHONE' => GetMessage("GET_PHONE"),
			'GET_START_MSG' => GetMessage("GET_START_MSG"),
			'GET_DELIMITER_MSG' => GetMessage("GET_DELIMITER_MSG"),
			'ADD_ADDRESS' => GetMessage("ADD_ADDRESS"),
			'FAQ' => GetMessage("FAQ"),
        ];
		
		if(SITE_CHARSET == 'windows-1251')
			$this->actionBoard = \Bitrix\Main\Text\Encoding::convertEncoding($this->actionBoard, 'windows-1251', 'UTF-8');
    }

    public function getOption($sectionId = false, $optionId = false)
    {
        return isset($this->options[$sectionId][$optionId]) ? $this->options[$sectionId][$optionId] : false;
    }

    public function getCase($caseId)
    {
        return isset($this->cases[$caseId]) ? $this->cases[$caseId] : false;
    }

    public function getAction($action)
    {
        return isset($this->actionBoard[$action]) ? $this->actionBoard[$action] : false;
    }

    public function getTitle($titleId)
    {
        return isset($this->titles[$titleId]) ? $this->titles[$titleId] : false;
    }
   
	public function getKeyboard()
    {
        return [
            [$this->actionBoard['getCatalog']],
			//[$this->actionBoard['getServices']],
            [$this->actionBoard['getStrakh']],
			//[$this->actionBoard['getFaq']],
			[$this->actionBoard['getHelp']],
        ];
    }

    public function getApiPath()
    {
        return __DIR__ . '/../include/botapi/vendor/autoload.php';
    }
	
}