<?
namespace Ingos\Bot;

class Utils
{
	public static function getCallBackStr($callBackArray = [])
	{
		if(!is_array($callBackArray) || empty($callBackArray)) return false;
		
		$returnStr = '';
		foreach($callBackArray as $k=>$v)
			$returnStr .= $k . ':' . $v . ';';
			
		return $returnStr;
	}

	public static function callBackToArray($callbackStr = '')
	{
		if($callbackStr == '' || !$callbackStr) return false;
		$callbackStr = explode(';', $callbackStr);
		$returnArr = [];
		foreach($callbackStr as &$callBackOption)
		{
			$tmp = explode(':', $callBackOption);
			$returnArr[$tmp[0]] = $tmp[1];
		}
		return $returnArr;
	}
	
	public static function isEmptyArray($arr = [])
	{
		if($arr == false) return true;
		if(count($arr) == 0) return true;
		
		$emptyValues = true;
		foreach($arr as $a)
			if($a) $emptyValues = false;
			
		return $emptyValues;
	}

	public static function getPass($number = 16)
	{
		$arr = ['a','b','c','d','e','f',
					 'g','h','i','j','k','l',
					 'm','n','o','p','r','s',
					 't','u','v','x','y','z',
					 'A','B','C','D','E','F',
					 'G','H','I','J','K','L',
					 'M','N','O','P','R','S',
					 'T','U','V','X','Y','Z',
					 '1','2','3','4','5','6',
					 '7','8','9','0','.',',',
					 '(',')','[',']','!','?',
					 '&','^','%','@','*','$',
					 '<','>','/','|','+','-',
					 '{','}','`','~'
				];
					 
		$pass = "";
		for($i = 0; $i < $number; $i++)
		{
		  $index = rand(0, count($arr) - 1);
		  $pass .= $arr[$index];
		}
		return $pass;
	}

	public static function getKeyboardRowButton($text = false, $callBack = '_')
	{
		return ['text' => $text, 'callback_data' => $callBack];
	}
	
	public static function addChatField()
	{
		$rsData = \CUserTypeEntity::GetList(array($by=>$order), array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_ITANDYR_CHAT'));
		if(!$rsData->GetNext())
		{
			$oUserTypeEntity = new \CUserTypeEntity();
			$nameField = ['ru' => 'UF_ITANDYR_CHAT', 'en' => 'UF_ITANDYR_CHAT'];
			$aUserFields = [
				'ENTITY_ID' => 'USER',
				'FIELD_NAME' => 'UF_ITANDYR_CHAT',
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'XML_ID' => 'XML_ITANDYR_CHAT',
				'SORT' => 500,
				'MANDATORY' => 'N',
				'SHOW_FILTER' => 'N',
				'SHOW_IN_LIST' => '',
				'EDIT_IN_LIST' => 'N',
				'IS_SEARCHABLE' => 'N',
				'EDIT_FORM_LABEL'   => $nameField,
				'LIST_COLUMN_LABEL' => $nameField,
				'LIST_FILTER_LABEL' => $nameField,
				'SETTINGS'          => [
					'DEFAULT_VALUE' => 'N',
					'SIZE' => '1',
					'ROWS' => '1',
					'MIN_LENGTH' => '1',
					'MAX_LENGTH' => '1',
					'REGEXP'  => '',
				]
			];
	 
			$iUserFieldId = $oUserTypeEntity->Add($aUserFields);
		}
	}

	public static function addOrderUserField()
	{
		$rsData = \CUserTypeEntity::GetList(array($by=>$order), array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_ITANDYR_IS_ORDER'));
		if(!$rsData->GetNext())
		{
			$oUserTypeEntity = new \CUserTypeEntity();
			$nameField = ['ru' => 'UF_ITANDYR_IS_ORDER', 'en' => 'UF_ITANDYR_IS_ORDER'];
			$aUserFields = [
				'ENTITY_ID' => 'USER',
				'FIELD_NAME' => 'UF_ITANDYR_IS_ORDER',
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'XML_ID' => 'XML_ITANDYR_IS_ORD',
				'SORT' => 500,
				'MANDATORY' => 'N',
				'SHOW_FILTER' => 'N',
				'SHOW_IN_LIST' => '',
				'EDIT_IN_LIST' => 'N',
				'IS_SEARCHABLE' => 'N',
				'EDIT_FORM_LABEL'   => $nameField,
				'LIST_COLUMN_LABEL' => $nameField,
				'LIST_FILTER_LABEL' => $nameField,
				'SETTINGS'          => [
					'DEFAULT_VALUE' => 'N',
					'SIZE' => '1',
					'ROWS' => '1',
					'MIN_LENGTH' => '1',
					'MAX_LENGTH' => '1',
					'REGEXP'  => '',
				]
			];
	 
			$iUserFieldId = $oUserTypeEntity->Add($aUserFields);
		}
		
		$rsData = \CUserTypeEntity::GetList(array($by=>$order), array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_ITANDYR_LAST_ORD'));
		if(!$rsData->GetNext())
		{
			$oUserTypeEntity = new \CUserTypeEntity();
			$nameField = ['ru' => 'UF_ITANDYR_LAST_ORD', 'en' => 'UF_ITANDYR_LAST_ORD'];
			$aUserFields = [
				'ENTITY_ID' => 'USER',
				'FIELD_NAME' => 'UF_ITANDYR_LAST_ORD',
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'XML_ID' => 'XML_ITANDYR_LAST_ORD',
				'SORT' => 500,
				'MANDATORY' => 'N',
				'SHOW_FILTER' => 'N',
				'SHOW_IN_LIST' => '',
				'EDIT_IN_LIST' => 'N',
				'IS_SEARCHABLE' => 'N',
				'EDIT_FORM_LABEL'   => $nameField,
				'LIST_COLUMN_LABEL' => $nameField,
				'LIST_FILTER_LABEL' => $nameField,
				'SETTINGS'          => [
					'DEFAULT_VALUE' => 'N',
					'SIZE' => '20',
					'ROWS' => '1',
					'MIN_LENGTH' => '0',
					'MAX_LENGTH' => '0',
					'REGEXP'  => '',
				]
			];
	 
			$iUserFieldId = $oUserTypeEntity->Add($aUserFields);
		}
		
		$rsData = \CUserTypeEntity::GetList(array($by=>$order), array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_ITANDYR_FIELD'));
		if(!$rsData->GetNext())
		{
			$oUserTypeEntity = new \CUserTypeEntity();
			$nameField = ['ru' => 'UF_ITANDYR_FIELD', 'en' => 'UF_ITANDYR_FIELD'];
			$aUserFields = [
				'ENTITY_ID' => 'USER',
				'FIELD_NAME' => 'UF_ITANDYR_FIELD',
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'XML_ID' => 'XML_ITANDYR_FIELD',
				'SORT' => 500,
				'MANDATORY' => 'N',
				'SHOW_FILTER' => 'N',
				'SHOW_IN_LIST' => '',
				'EDIT_IN_LIST' => 'N',
				'IS_SEARCHABLE' => 'N',
				'EDIT_FORM_LABEL'   => $nameField,
				'LIST_COLUMN_LABEL' => $nameField,
				'LIST_FILTER_LABEL' => $nameField,
				'SETTINGS'          => [
					'DEFAULT_VALUE' => 'N',
					'SIZE' => '20',
					'ROWS' => '1',
					'MIN_LENGTH' => '0',
					'MAX_LENGTH' => '0',
					'REGEXP'  => '',
				]
			];
	 
			$iUserFieldId = $oUserTypeEntity->Add($aUserFields);
		}
		
	}
	
	public static function getLocArray($arrLoc = [])
	{
		$lon = $arrLoc['lon'] ? : false;
		$lat = $arrLoc['lat'] ? : false;
		
		if(!$lon || !$lat) return false;
		
		
		$location = simplexml_load_file("https://geocode-maps.yandex.ru/1.x/?geocode={$lon},{$lat}");
		
		if($location !== FALSE)
		{
			
			$arReturn = [];
			$location = self::xml2array($location);
			$foundPoints = $location['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'];
			
			if($foundPoints > 0)
			{
				$featureMember = self::xml2array($location['GeoObjectCollection']['featureMember'][0]);
				
				if(SITE_CHARSET == 'windows-1251')
					$locationItems = \Bitrix\Main\Text\Encoding::convertEncoding($featureMember['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country'], 'UTF-8', 'windows-1251');
				
				$arReturn['ZIP'] = $featureMember['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['postal_code'];
				$arReturn['ADDRESS'] = $locationItems['AddressLine'];
				
				$arReturn['COUNTRY'] = $locationItems['CountryName'];
				$arReturn['REGION'] = $locationItems['AdministrativeArea']['AdministrativeAreaName'];
				$arReturn['CITY'] =  $locationItems['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName']?:$arReturn['REGION'];
				
				return $arReturn;
			}
			else
			{
				return false;
			}
		}
	}
	
	public static function setUserField($code = '', $value = '')
	{
		global $USER;
		$uid = $USER->GetID();
		$GLOBALS["USER_FIELD_MANAGER"]->Update('USER', $uid, [$code => $value]);
	}

	public static function getKeyOrderUserField($values = '', $code = '')
	{
		foreach($values as $key=>$value)
		{
			$propValue = explode('=>', $value);
			if($propValue[0] === $code)
				return $key;
		}

		return false;
	}

	public static function getArrayFromOrderField($values = '')
	{
		$returnArr = [];
		foreach($values as $key=>$value)
		{
			$propValue = explode('=>', $value);
			$returnArr[$propValue[0]] = $propValue[1];
		}

		return $returnArr;
	}
	
	public static function xml2array($xmlObject)
	{
		$out = [];
		
		foreach ( (array) $xmlObject as $index => $node )
			$out[$index] = ( is_object ( $node ) ) ? self::xml2array ( $node ) : $node;
	
		return $out;
	}
}