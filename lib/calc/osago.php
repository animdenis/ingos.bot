<?
namespace Ingos\Bot\Calc;

use \Ingos\Bot\Msg as Msg;
use \Ingos\Bot\Utils as Utils;

class Osago
{
	const FIRST_FIELD = 'person_type';
	const CLASS_NAME = 'Osago';
	
	private static function createField($fieldCode)
	{
		$fields = self::readFields();
		switch($fieldCode)
		{
			case 'person_type':
				return new Msg('�������� ��� ������������:', [
					[['text' => '���������� ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 1, 'class' => self::CLASS_NAME,])]],
					[['text' => '����������� ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 2, 'class' => self::CLASS_NAME,])]]
				]);
			break;
			
			case 'city':
				return new Msg('������� ����� ��� ��������� ���� ��������������, � ��������� ���������� �����.', 'location');
			break;
			
			case 'category':
				$keyboard = [
					[['text' => 'A - ���������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'A', 'class' => self::CLASS_NAME,])]],
					[['text' => 'B - ��������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'B', 'class' => self::CLASS_NAME,])]],
					[['text' => 'C - ��������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'C', 'class' => self::CLASS_NAME,])]],
					[['text' => 'D - ��������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'D', 'class' => self::CLASS_NAME,])]],
					[['text' => 'E - ��������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'E', 'class' => self::CLASS_NAME,])]]
				];
				if($fields['person_type'] == 2)
				{
					$keyboard[] = [['text' => 'Tb - �����������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'Tb', 'class' => self::CLASS_NAME,])]];
					$keyboard[] = [['text' => 'Tm - �������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'Tm', 'class' => self::CLASS_NAME,])]];
				}
				
				return new Msg('�������� ���������:', $keyboard);
			break;
				case 'trailer':
					return new Msg('������������� � ��������?:', [
						[['text' => '� ��������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 1, 'class' => self::CLASS_NAME,])]],
						[['text' => '��� �������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 0, 'class' => self::CLASS_NAME,])]]
					]);
				break;
				
					case 'aim':
						
						if($fields['person_type'] == 2)
							return new Msg('���� ������������� ��:', [
								[['text' => '������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'personal', 'class' => self::CLASS_NAME,])]],
								[['text' => '������� ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'study', 'class' => self::CLASS_NAME,])]],
								[['text' => '������/������/����������� ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'sharing', 'class' => self::CLASS_NAME,])]],
								[['text' => '��������� ������� � ����� �����. ������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'danger', 'class' => self::CLASS_NAME,])]],
								[['text' => '�������� � ����������� ��', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'road', 'class' => self::CLASS_NAME,])]],
								[['text' => '���������� � ������������ ������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'extra', 'class' => self::CLASS_NAME,])]],
								[['text' => '', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'other', 'class' => self::CLASS_NAME,])]]
							]);
						else
							return new Msg('���� ������������� ��:', [
								[['text' => '������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'personal', 'class' => self::CLASS_NAME,])]],
								[['text' => '������� ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'study', 'class' => self::CLASS_NAME,])]],
								[['text' => '������', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'other', 'class' => self::CLASS_NAME,])]]
							]);
					break;
				case 'weight':
					return new Msg('����������� ������������ �����:', [
						[['text' => '�� 16 ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'before', 'class' => self::CLASS_NAME,])]],
						[['text' => '����� 16 ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'after', 'class' => self::CLASS_NAME,])]]
					]);
				break;
				case 'qty':
					return new Msg('���������� ������������ ����:', [
						[['text' => '����� 16 ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'before', 'class' => self::CLASS_NAME,])]],
						[['text' => '����� 16 ����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'after', 'class' => self::CLASS_NAME,])]]
					]);
				break;
			case 'mark':
				return new Msg('����� ����������:');
			break;
			case 'model':
				return new Msg('������ ����������:');
			break;
			case 'year':
				return new Msg('��� �������:');
			break;
			case 'power':
				return new Msg('���������� �.�.:');
			break;
			
			case 'infinity_drivers':
				if($fields['person_type'] == 2)
				{
					self::setField(1);
					return self::getField();
				}
				return new Msg('��� ����������� ���������:', [
					[['text' => '��', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 1, 'class' => self::CLASS_NAME,])]],
					[['text' => '���', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 0, 'class' => self::CLASS_NAME,])]]
				]);
			break;
			case 'drivers':
				if($fields['infinity_drivers'])
				{
					$fields['drivers'] = -1;
					self::writeFields($fields);
					return self::getField();
				}
				return new Msg('���������� ���������:');
			break;
			case 'drivers_array':
				if($fields['drivers'] >= 1)
				{
					$cnt = count($fields['drivers_array']) + 1;
					return new Msg("������� ������ {$cnt} �������� ����� �������. ���, ���� ��������, ����� � ����� �������������, ���� ������ �����.\n<b>������</b>: ������ ���� ��������, 11.02.1995, 1243 33321, 12.02.2015");
				}
				elseif($fields['drivers'] == -1)
				{
					$fields['drivers_array'] = [];
					self::writeFields($fields);
					return self::getField();
				}
			break;
			case 'owner':
				if($fields['drivers'] == -1)
				{
					return new Msg("������� ������ ������������ ����� �������. ���, ���� ��������, ������� �� (����� � �����), VIM.\n<b>������</b>: ������ ���� ��������, 11.02.1995, 1243 33321, 12334431");
				}
				else
				{
					$fields['owner'] = '';
					self::writeFields($fields);
					return self::getField();
				}
			break;
			case 'period_type':
				return new Msg('�������� ������ �����������:', [
					[['text' => '������� ������� �� 1 ���', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'year', 'class' => self::CLASS_NAME,])]],
					[['text' => '��������� �����', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'time', 'class' => self::CLASS_NAME,])]],
					[['text' => '�� 20 ���� - ���������� � ����� ���. ��', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => 'short', 'class' => self::CLASS_NAME,])]],
				]);
			break;
			case 'period':
				return new Msg('������� ���� ������ ������� � ������� ��.��.���� (������: 11.02.2014)');
			break;
			case 'contacts':
				return new Msg('��������� ��� �������. �� ������ ��������������� ����������� ��������� ���������.', 'contacts');
			break;
			case 'time_to_call':
				return new Msg('����� � ���� ����� ����� ��������� (������: 11.03.2017 18:30)? �� �������� � 9-00 �� 18-00 �� ���, ���������, ��-�� ��������.', [
					[['text' => '��������� ����� - 11.03.2017 18:30', 'callback_data' => Utils::getCallBackStr(['case' => 'calcProcess', 'value' => '11.03.2017 18:30', 'class' => self::CLASS_NAME,])]]
				]);
			break;
		}
	}
	
	private static function readFields()
	{
		global $USER;
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $USER->GetID());
		return unserialize($userFields['UF_CALC_VALUE']['VALUE']);
	}
	
	private static function clearFields()
	{
		Utils::setUserField('UF_CALC_VALUE', '');
		return self::readFields();
	}
	
	private static function writeFields($fields)
	{
		Utils::setUserField('UF_CALC_VALUE', serialize($fields));
	}
	
	private static function getCurrentFieldCode($fields = false)
	{
		if(!$fields) $fields = self::readFields();
		
		global $USER;
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $USER->GetID());
		if(!empty($userFields['UF_CALC_CAR']['VALUE'])) return $userFields['UF_CALC_CAR']['VALUE'];
		
		$fieldCode = [
			'person_type',
			'city',
			'category',
			'infinity_drivers',
			'drivers',
			'drivers_array',
			'owner',
			'period_type',
			'period',
			'contacts',
			'time_to_call'
		];
		foreach($fieldCode as $fCode)
		{
			if($fields['drivers'] >= 1 && count($fields['drivers_array']) < $fields['drivers'])
				return 'drivers_array';
			
			if(!isset($fields[$fCode]))
				return $fCode;
		}
			
		return false;
	}
	
	public static function getField($isStart)
	{
		$fields = $isStart ? self::clearFields() : self::readFields();
		
		if(empty($fields))
			return self::createField(self::FIRST_FIELD);
		
		if($code = self::getCurrentFieldCode($fields))
			return self::createField($code);
		
		return self::doCalc();
	}
	
	public static function setField($value)
	{
		$fields = self::readFields();
		if(empty($fields))
		{
			if(!self::checkField($value, self::FIRST_FIELD)) return false;
			$fields[self::FIRST_FIELD] = $value;
		}
		elseif($code = self::getCurrentFieldCode($fields))
		{
			if(!self::checkField($value, $code)) return false;

			switch($code)
			{
				case 'category': 
					switch ($value)
					{
						case 'A': 
						case 'E':
						case 'Tb':
						case 'Tm':
							$nextField = 'trailer';
						break;
						case 'B': $nextField = 'mark'; break;
						case 'C': $nextField = 'weight'; break;
						case 'D': $nextField = 'qty'; break;
					}
				break;
				case 'trailer':
					$nextField = 'aim';
				break;
				case 'weight':
				case 'qty':
				case 'power':
					$nextField = 'trailer';
				break;
				case 'mark':
					$nextField = 'model';
				break;
				case 'model':
					$nextField = 'year';
				break;
				case 'year':
					$nextField = 'power';
				break;
				case 'drivers_array':
					if($fields['drivers'] >= 1 && count($fields['drivers_array']) < $fields['drivers'])
					{
						$fields[$code][] = $value;
						self::writeFields($fields);
						return true;
					}
				break;
				default: $nextField = '';
			}
			
			$fields[$code] = $value;
			Utils::setUserField('UF_CALC_CAR', $nextField);
		}
		self::writeFields($fields);
		return true;
	}
	
	public static function setResult($result)
	{
		$fields = self::readFields();
		$fields['result'] = $result;
		self::writeFields($fields);
		
		return new Msg('�������! �� � ���� �������� � ��������� ���� �����!');
	}
	
	public static function setLocation($value)
	{
		$locArray = Utils::getLocArray($value);
		self::setField($locArray['CITY']);
	}
	
	public static function checkField($value, $fieldCode)
	{
		switch($fieldCode)
		{
			case 'person_type':
				return in_array($value, ['1', '2']);
			break;
			case 'trailer':
			case 'infinity_drivers':
				return in_array($value, ['1', '0']);
			break;
				case 'city':
					return $value !== NULL;
				break;
				
				case 'category':
					return in_array($value, ['A', 'B', 'C', 'D', 'E', 'Tb', 'Tm']);
				break;
					case 'aim':
						return in_array($value, ['personal', 'study', 'sharing', 'danger', 'road', 'extra', 'other']);
					break;
					case 'weight':
					case 'qty':
						return in_array($value, ['before', 'after']);
					break;
				case 'mark':
					return $value !== NULL;
				break;
				case 'model':
					return $value !== NULL;
				break;
				case 'year':
					return (int)$value > 1970 && (int)$value <= (int)date('Y');
				break;
				case 'power':
					return (int)$value > 40 && (int)$value <= 1200;
				break;
			case 'drivers':
				return (int)$value >= 1 && (int)$value <= 10;
			break;
			case 'drivers_array':
				return $value !== NULL;
			break;
			case 'owner':
				return $value !== NULL;
			break;
			case 'period_type':
				return in_array($value, ['year', 'time', 'short']);
			break;
			case 'period':
				return $value !== NULL;
			break;
			case 'contacts':
				return $value !== NULL;
			break;
			case 'time_to_call':
				return $value !== NULL;
			break;
			default: return false;
		}
	}
	
	public static function doCalc()
	{
		//read fields
		//calculate
		//output variants
		return new Msg('�� ��������� ��������� ������ (������������ ������ �����������):', [
			[['text' => '10 000 ���.', 'callback_data' => Utils::getCallBackStr(['case' => 'calcResult', 'value' => '10000', 'class' => self::CLASS_NAME])]],
			[['text' => '20 000 ���.', 'callback_data' => Utils::getCallBackStr(['case' => 'calcResult', 'value' => '20000', 'class' => self::CLASS_NAME])]],
			[['text' => '30 000 ���.', 'callback_data' => Utils::getCallBackStr(['case' => 'calcResult', 'value' => '30000', 'class' => self::CLASS_NAME])]]
		]);
	}
}