<?
namespace Itandyr\Salebot;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use \Itandyr\Salebot\Options as Opt;

Loader::includeModule("catalog");
Loader::includeModule("sale");
Loader::includeModule("iblock");

class Basket
{
	private $opt = false;

    public function __construct()
    {
        global $botOptions;
        $this->opt = $botOptions;
    }
	
	/*PROTECTED FUNCTIONS*/
		protected function _getBasketItemMenu($basketItem = false, $arNav = [], $sum = 0, $arAllBasket = false, $currency = 'RUB')
		{
			$format = $basketItem['QUANTITY'] - floor($basketItem['QUANTITY']) > 0 ? 1 : 0;
			
			$arBasketItemMenu = [];
			$arBasketItemMenu[] = [Utils::getKeyboardRowButton($this->_getItemSum($basketItem, $currency))];
			$arBasketItemMenu[] = [
				Utils::getKeyboardRowButton('-', Utils::getCallBackStr([
					'case' => $this->opt->getCase('changeQty'),
					'id' => $basketItem['ID'],
					'action' => 'min',
					'nElNum' => $arNav['nElNum'],
					'sumEl' => $arNav['sumEl'],
					'measure' => $basketItem['MEASURE']
				])),
				Utils::getKeyboardRowButton(implode(' ', [number_format($basketItem['QUANTITY'], $format), $basketItem['MEASURE_NAME']])),
				Utils::getKeyboardRowButton('+', Utils::getCallBackStr([
					'case' => $this->opt->getCase('changeQty'),
					'id' => $basketItem['ID'],
					'action' => 'plus',
					'nElNum' => $arNav['nElNum'],
					'sumEl' => $arNav['sumEl'],
					'measure' => $basketItem['MEASURE']
				])),
				Utils::getKeyboardRowButton('x', Utils::getCallBackStr([
					'case' => $this->opt->getCase('delBasket'),
					'id' => $basketItem['ID'],
					'nElNum' => $arNav['nElNum'],
					'sumEl' => $arNav['sumEl']
				])),
			];
			
			if($arNav && !Utils::isEmptyArray($arNav) && $arAllBasket)
			{
				$arNav['IDs'] = $this->_getBasketIDs($basketItem['ID'], $arAllBasket);
				//AddMessage2Log($arNav['IDs']);
				$arNavKeys = CatalogUtils::getElementNav($this->opt->getCase('getBasketItem'), $basketItem['ID'], $arNav);
				$arBasketItemMenu = array_merge($arBasketItemMenu, $arNavKeys);
			}
			
			if($currency !== $basketItem['CURRENCY'])
				$sum = \CCurrencyRates::ConvertCurrency($sum, $basketItem['CURRENCY'], $currency);

			$arBasketItemMenu[] = [Utils::getKeyboardRowButton(GetMessage('SUM') . \CCurrencyLang::CurrencyFormat($sum, $currency,TRUE))];
			$arBasketItemMenu[] = [
				Utils::getKeyboardRowButton(GetMessage('CREATE_ORDER'), Utils::getCallBackStr([
					'case' => $this->opt->getCase('createOrder')
				])),
				Utils::getKeyboardRowButton(GetMessage('CLEAR_BASKET'), Utils::getCallBackStr([
					'case' => $this->opt->getCase('clearBasket')
				])),
			];
			
			return $arBasketItemMenu;
		}

			protected function _getItemSum($basketItem, $currency)
			{
				$price = $basketItem['OPTIMAL_PRICE']['RESULT_PRICE']['DISCOUNT_PRICE']?:$basketItem['PRICE'];
				
				if($currency !== $basketItem['CURRENCY'])
					$price = \CCurrencyRates::ConvertCurrency($price, $basketItem['CURRENCY'], $currency);
				
				$basketItem['CURRENCY'] = $currency;
				$format = $basketItem['QUANTITY'] - floor($basketItem['QUANTITY']) > 0 ? 1 : 0;
				$allSum = 	\CCurrencyLang::CurrencyFormat(
										$price, 
										$basketItem['CURRENCY'],
										TRUE
						) . ' * '.	implode('', [number_format($basketItem['QUANTITY'], $format), $basketItem['MEASURE_NAME']]) . ' = ' . \CCurrencyLang::CurrencyFormat(
										$price * $basketItem['QUANTITY'], 
										$basketItem['CURRENCY'],
										TRUE
						);
				
				return $allSum;
			}

			protected function _getBasketIDs($basketItemId = false, $arAllBasket = false)
			{			
				$basketItemIDs = [];
				$key = 0;
				
				foreach($arAllBasket as $obBasketItem)
				{
					$basketItemIDs[] = $obBasketItem['ID'];
					if($obBasketItem['ID'] == $basketItemId)
						$key = count($basketItemIDs) - 1;
				}
				
				$countItems = count($arAllBasket);
				if($countItems == 2)
					return $basketItemIDs;
				
				if($countItems > 2)
				{
					$navIDs = array_slice($basketItemIDs, $key - 1, 3);
					
					if($key == 0)
						$navIDs = array_slice($basketItemIDs, $key, 2);
					
					if($key == $countItems - 1)
						$navIDs = array_slice($basketItemIDs, $key - 1, 2);
					 
					return is_array($navIDs) ? $navIDs : false;
				}
				
				return false;
			}
		
	/**/
	
	public function getBasket($basketItem = false, $arNav = false)
	{
		$basketItem = (int)$basketItem > 0 ? (int)$basketItem : false;
		global $botOptions;
		global $USER;
		
		$basketClass = new \CSaleBasket();
		$dbBasketItems = $basketClass->GetList(array("ID" => "DESC"), array("FUSER_ID" => \CSaleBasket::GetBasketUserID(), "LID" => $this->opt->getOption('main', 'bot_site_id'), "ORDER_ID" => "NULL"), false, false, array());
		
		$countItems = $dbBasketItems->SelectedRowsCount();
		if($countItems == 0) return new Msg(GetMessage('BASKET_IS_EMPTY'));

		$arMainItem = false;
		$sum = 0;
		$arAllBasket = [];
		$tmp = false;
		while ($arItem = $dbBasketItems->Fetch())
		{
			$tmp = $arItem;
			$arAllBasket[]['ID'] = $arItem['ID'];
			
			if(($basketItem == $arItem['ID'] || !$basketItem) && !$arMainItem)
			{
				$arItem['OPTIMAL_PRICE'] = \CCatalogProduct::GetOptimalPrice($arItem['PRODUCT_ID'], 1, $USER->GetUserGroupArray());
				$arItem['MEASURE'] = CatalogUtils::getMeasure($arItem['PRODUCT_ID']);
				$arMainItem = $arItem;
			}

			$optimalPrice = \CCatalogProduct::GetOptimalPrice($arItem['PRODUCT_ID'], $arItem['QUANTITY'], $USER->GetUserGroupArray());
			if($optimalPrice)
				$sum += $optimalPrice['RESULT_PRICE']['DISCOUNT_PRICE'] * $arItem['QUANTITY'];
		}
		
		if(!$arMainItem) return new Msg(GetMessage('ERR_ITEM_READ'));
	
		if(!$arNav && ($countItems > 0))
			$arNav = ['nElNum' => 1,'sumEl' => $countItems];


		//$catalog = new \Itandyr\Salebot\Catalog();
		$text = $this->_getBasketElement($arMainItem['PRODUCT_ID']);
		$keyboard = $this->_getBasketItemMenu($arMainItem, $arNav, $sum, $arAllBasket, $botOptions->getOption('catalog', 'bot_catalog_currency'));
		//AddMessage2Log($keyboard);
		return new Msg($text, $keyboard);
	}
	
		protected function _getBasketElement($elementId = false)
		{
			 $arFilter = [
				'IBLOCK_ID' => $this->opt->getOption('catalog', 'bot_catalog_iblock'),
				'ID' => $elementId
			];
			//photo props
			$photoProp = $this->opt->getOption('catalog', 'bot_catalog_element_photoprop');
			if($photoProp !== 'NOT' && $photoProp !== NULL)
				$photoProp = ['PROPERTY_' . $photoProp];
			else
				$photoProp = [];
			
			$dbElement = \CIblockElement::GetList($sort, $arFilter, false, false, $arSelect);
			$elMenu = [];
			if($obElement = $dbElement->GetNext())
			{
				return CatalogUtils::getElementName($obElement);
			}
			else
			{
				return new Msg('ELEMENT_NOT_FOUND');
			}
		}
		
	public function changeQty($basketItem = false, $action = false, $measure = 1)
	{
		$basketClass = new \CSaleBasket();
		$arBasketItem = $basketClass->GetById($basketItem);
		//AddMessage2Log($measure);
		if($arBasketItem['CAN_BUY'] == 'Y')
		{
			if($action == 'plus')
			{
				$basketClass->Update($arBasketItem['ID'], [ "QUANTITY" => $arBasketItem['QUANTITY'] + $measure]);
			}
			if($action == 'min' && (int)$arBasketItem['QUANTITY'] > $measure)
			{
				$basketClass->Update($arBasketItem['ID'], [ "QUANTITY" => $arBasketItem['QUANTITY'] - $measure]);
			}
		}
	}
	
	public function addBasket($elementId = false, $measure = 1)
	{
		Add2BasketByProductID($elementId, $measure);
	}
	
	public function delBasket($basketItemId, $arNav = [])
	{
		$basketClass = new \CSaleBasket();
		$basketId = $basketClass->Delete($basketItemId);
	}
	
	public function clearBasket()
	{
		$basketClass = new \CSaleBasket();
		$basketClass->DeleteAll(\CSaleBasket::GetBasketUserID());
	}
}