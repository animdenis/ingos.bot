<?
namespace Ingos\Bot;

class User
{	
	public function __construct($chatUser = [], $chatId)
	{	
		global $USER;
		global $botOptions;
		
		$rsUser = $USER->GetByLogin($chatUser['id']);
		if($arUser = $rsUser->Fetch())
			$USER->Authorize($arUser['ID'], true);
		
		$this->updateChatId($chatId);
		
		$pass = Utils::getPass();
		$arFields = [
		  "NAME"              => $chatUser['firstname'],
		  "LAST_NAME"         => $chatUser['lastname'],
		  "EMAIL"             => $chatUser['id'] . "@t.me",
		  "LOGIN"             => $chatUser['id'],
		  "ACTIVE"            => "Y",
		  "GROUP_ID"          => explode(',', $botOptions->getOption('main', 'bot_user_group')),
		  "PASSWORD"          => $pass,
		  "CONFIRM_PASSWORD"  => $pass,
		];

		$ID = $USER->Add($arFields);
		if (intval($ID) > 0)
			$USER->Authorize($ID);
		else
			return false;
	}
	
	public function updateChatId($chatId)
	{
		Utils::addChatField();
		Utils::setUserField('UF_ITANDYR_CHAT', $chatId);
	}
	
	public function getCalcClass()
	{
		global $USER;
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $USER->GetID());
		return $userFields['UF_CALC_CLASS']['VALUE'] !== '' ? $userFields['UF_CALC_CLASS']['VALUE'] : false;
	}
}