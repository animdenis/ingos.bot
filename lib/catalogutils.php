<?
namespace Ingos\Bot;

use Bitrix\Main\Loader;
use \Ingos\Bot\Options as Opt;

Loader::includeModule("catalog");
Loader::includeModule("sale");

class CatalogUtils
{
	public static function getPrice($elementId = false)
	{
		if(!$elementId) return false;
		global $botOptions;

		global $USER;
		$arPrice = \CCatalogProduct::GetOptimalPrice($elementId, 1, $USER->GetUserGroupArray());
		
		if($arPrice['RESULT_PRICE']['CURRENCY'] !== $botOptions->getOption('catalog', 'bot_catalog_currency'))
			$arPrice['RESULT_PRICE']['DISCOUNT_PRICE'] = \CCurrencyRates::ConvertCurrency($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'], $arPrice['RESULT_PRICE']['CURRENCY'], $botOptions->getOption('catalog', 'bot_catalog_currency'));
		
		$arPrice['RESULT_PRICE']['CURRENCY'] = $botOptions->getOption('catalog', 'bot_catalog_currency');
		
		$priceStr = '';
		if($arPrice)
		{
			$priceStr = \CCurrencyLang::CurrencyFormat(
				$arPrice['RESULT_PRICE']['DISCOUNT_PRICE'], 
				$arPrice['RESULT_PRICE']['CURRENCY'],
				TRUE
			);
		}

		return $priceStr;
	}

	public static function getDeliveryPrice($arDelivery)
	{
		if(!$arDelivery['PRICE'] || !$arDelivery['CURRENCY']) return false;

		$priceStr = \CCurrencyLang::CurrencyFormat(
				$arDelivery['PRICE'], 
				$arDelivery['CURRENCY'],
				TRUE
		);

		return $priceStr;
	}

	public static function addOrderProperty($code, $value, $order)
	{
		if (!strlen($code)) return false;     
		
		if ($arProp = \CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch())
		{
			return \CSaleOrderPropsValue::Add(array(
			   'NAME' => $arProp['NAME'],
			   'CODE' => $arProp['CODE'],
			   'ORDER_PROPS_ID' => $arProp['ID'],
			   'ORDER_ID' => $order,
			   'VALUE' => $value,
			));
		}
   }

   public static function getPayLink($orderId = 0)
   {
   		if(!$orderId) return false;

		global $botOptions;
		global $USER;
		
		$redirectUrl = $botOptions->getOption('main', 'bot_redirect');
		//AddMessage2Log($redirectUrl);
		$siteUrl = $botOptions->getOption('main', 'bot_siteurl');
		$pathToComponent = $botOptions->getOption('order', 'bot_order_component') . '?ORDER_ID=' . $orderId;
		
		$login = $USER->GetLogin();
		$hash = $USER->GetParam("PASSWORD_HASH");
		$redirect = $siteUrl . $pathToComponent;

		return $siteUrl . "{$redirectUrl}?UID={$login}&AUTH={$hash}&REDIRECT={$redirect}";
   }

	public static function getMeasure($elementId = false)
	{
		if(!$elementId) return false;

		$dbMeasure = \CCatalogMeasureRatio::getList([],['PRODUCT_ID' => $elementId]);
        $obMeasure = $dbMeasure->GetNext();
		
		return $obMeasure ? $obMeasure['RATIO'] : 1;
	}
	
	public static function getCountItem($elementId)
	{
		if(!$elementId) return false;

		global $botOptions;
		
		$dbBasketItems = \CSaleBasket::GetList([],array(
			"FUSER_ID" => \CSaleBasket::GetBasketUserID(),
			"LID" => $botOptions->getOption('main', 'bot_site_id'),
			"ORDER_ID" => "NULL"
		),false,false,array('PRODUCT_ID', 'QUANTITY'));

		$cnt = 0;
		while($obBasket = $dbBasketItems->Fetch())
			if($obBasket['PRODUCT_ID'] == $elementId)
				$cnt = $obBasket['QUANTITY'];
				
		if($cnt > 0)
			return " ({$cnt})";
	}

	public static function getSortArray($type = 'ELEMENT_LIST')
    {
    	if(!$type) return false;

    	global $botOptions;
        switch ($type)
        {
            case('ELEMENT_LIST'):
                return [$botOptions->getOption('catalog', 'bot_catalog_sort_element') => $botOptions->getOption('catalog', 'bot_catalog_by_element')];
            break;

            case('SECTION_LIST'):
                return [$botOptions->getOption('catalog', 'bot_catalog_sort_section') => $botOptions->getOption('catalog', 'bot_catalog_by_section')];
            break;

            default:
            	return ['SORT' => 'asc'];

        }
    }
		
	public static function getElementName($obElement = [])
	{
		if(!$obElement["NAME"]) return false;

		global $botOptions;

		$photoProp = $botOptions->getOption('catalog', 'bot_catalog_element_photoprop');
		$photoField = $botOptions->getOption('catalog', 'bot_catalog_element_photofields');

		$picId = false;
		if($photoProp !== 'NOT' && !empty($obElement['PROPERTY_' . $photoProp . '_VALUE']))
		{
			$picId = $obElement['PROPERTY_' . $photoProp . '_VALUE'];
		}
		else
		{
			switch ($photoField) {
				case 'DETAIL_PICTURE':
					$picId = $obElement["DETAIL_PICTURE"] ?: $obElement["PREVIEW_PICTURE"];
				break;
				case 'PREVIEW_PICTURE':
					$picId = $obElement["PREVIEW_PICTURE"] ?: $obElement["DETAIL_PICTURE"];
				break;
				default:
					$picId = false;
				break;
			}

		}
		
		$link = false;
		if($picId) $link = \CFile::GetPath($picId);
		
		$siteUrl = $botOptions->getOption('main', 'bot_siteurl');
		return $link ? '<a href="' . $siteUrl . $link . '">' . $obElement["NAME"] . '</a>' : $obElement["NAME"];
	}
	
	public static function getElementDescr($obElement = [])
	{
		global $botOptions;
		$stripTags = $botOptions->getOption('catalog', 'bot_catalog_element_htmlsplit') === 'Y';

		$descriptionText = '';
		switch ($botOptions->getOption('catalog', 'bot_catalog_element_descr')) {
			case 'DETAIL_TEXT':
					$descriptionText = $obElement['DETAIL_TEXT']?:$obElement['PREVIEW_TEXT'];
			break;
			case 'PREVIEW_TEXT':
					$descriptionText = $obElement['PREVIEW_TEXT']?:$obElement['DETAIL_TEXT'];
			break;
			default:
					$descriptionText =  $obElement['DETAIL_TEXT'];
			break;
		}

		$clearText = $stripTags ? strip_tags($descriptionText) : $descriptionText;

		$explodeText = explode(PHP_EOL, $clearText);
		$newExplode = [];
		foreach($explodeText as $text)
			if(trim($text) != '')
				$newExplode[] = trim($text);
			
		return implode(PHP_EOL . PHP_EOL, $newExplode);
	}
	
	public static function getElementProps($obElement = [], $arPropCode = [])
	{
		$arProps = [];
		foreach($arPropCode as $prop)
		{
			$val = $obElement['PROPERTY_' . $prop['CODE'] . '_VALUE'];
			if(!empty($val))
				$arProps[] = $prop['NAME'] . ': ' . $val;
		}
		
		return !empty($arProps) ? implode("\n", $arProps) : '';
	}

	public static function getElementNav($callBackCaseId = false, $callBackItemId = false, $arNav = [])
	{
		if(!$callBackCaseId || !$callBackItemId || !is_array($arNav)) return false;

		$pageNumber = $arNav['nElNum'];
		$sumPages = $arNav['sumEl'];
		$prevElId = $arNav['IDs'][0];
		$nextElId = isset($arNav['IDs'][2]) ? $arNav['IDs'][2] : $arNav['IDs'][1];
		$nextCallBackData =  Utils::getCallBackStr([
								'case' => $callBackCaseId,
								'id' => $nextElId == $callBackItemId ? $callBackItemId : $nextElId,
								'sumEl' => $sumPages,
								'nElNum' => $arNav['nElNum'] == $sumPages ? $sumPages : $arNav['nElNum'] + 1,
							]);
		$prevCallBackData = Utils::getCallBackStr([
								'case' => $callBackCaseId,
								'id' => $prevElId == $callBackItemId ? $callBackItemId : $prevElId,
								'sumEl' => $sumPages,
								'nElNum' => $arNav['nElNum'] == 1 ? $arNav['nElNum'] : $arNav['nElNum'] - 1,
							]);
							
		return self::_getNavString(
					$prevCallBackData,
					$nextCallBackData,
					$pageNumber,
					$sumPages
				);
	}
	
	public static function getPageNav($callBackCaseId = false, $callBackItemId = false, $currentPage = 1, $sumPages = false)
	{
		if(!$callBackCaseId || !$currentPage || !$sumPages) return [[[]]];

		$nextNumber = ($sumPages  == $currentPage) ? 1 : $currentPage + 1;
		$prevNumber = ($currentPage == 1) ? $sumPages : $currentPage - 1;

		$prevCallBackData = Utils::getCallBackStr([
								'case' => $callBackCaseId,
								'id' => $callBackItemId,
								'page' => $prevNumber
							]);
		$nextCallBackData = Utils::getCallBackStr([
								'case' => $callBackCaseId,
								'id' => $callBackItemId,
								'page' => $nextNumber
							]);
		
		return self::_getNavString(
					$prevCallBackData,
					$nextCallBackData,
					$currentPage,
					$sumPages
				);
	}
	
	private static function _getNavString($prevCallBackData, $nextCallBackData, $pageNumber, $sumPages)
	{
		return [[
			[
				'text' => '<<',
				'callback_data' => $prevCallBackData
			],
			[
				'text' => "{$pageNumber} / {$sumPages}",
				'callback_data' => $nextCallBackData
			],
			[
				'text' =>'>>',
				'callback_data' => $nextCallBackData
			],
		]];
	}


}