<?
namespace Itandyr\Salebot;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loader::includeModule("sale");

class Order
{	
	protected $opt = false;
	protected $orderFields = false;
	
	protected function _isNewUser($uid = false)
	{
		if($uid)
		{
			$dbOrders = \CSaleOrder::GetList(["DATE_INSERT" => "DESC"], ['USER_ID' => $uid]);
			$ordCount = $dbOrders->SelectedRowsCount();
			if($ordCount > 0) return false;
		}

		return true;
	}
	
	protected function _getOrderFieldsText($orderId = false)
	{
		if($orderId)
		{
			$dbProps = \CSaleOrderPropsValue::GetOrderProps($orderId);
			while ($arProps = $dbProps->Fetch())
			{
				if(!empty($arProps["VALUE"]) || $arProps["VALUE"] !== NULL)
				{
					$propsValue = GetMessage('PROP_NAME', ['#NAME#' => $arProps["NAME"]]);;
					if ($arProps["TYPE"]=="CHECKBOX")
					{
						if ($arProps["VALUE"] == "Y")
							$propsValue .= GetMessage('YES');
						else
							$propsValue .= GetMessage('NO');
					}
					elseif($arProps["TYPE"] == "TEXT" || $arProps["TYPE"] == "TEXTAREA")
					{
						$propsValue .= $arProps["VALUE"];
					}
					elseif($arProps["TYPE"] == "SELECT" || $arProps["TYPE"] == "RADIO")
					{
						$arVal = \CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $arProps["VALUE"]);
						$propsValue .= $arVal["NAME"];
					}
					elseif($arProps["TYPE"] == "MULTISELECT")
					{
						$curVal = split(GetMEssage('SPLIT_DELIMITER'), $arProps["VALUE"]);
						for ($i = 0; $i < count($curVal); $i++)
						{
							$arVal = \CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $curVal[$i]);
							if ($i > 0) $propsValue .= GetMessage('DELIMITER');
							$propsValue .= $arVal["NAME"];
						}
					}
					elseif($arProps["TYPE"] == "LOCATION")
					{
						$arVal = \CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
						$propsValue .= GetMessage('LOCATION', [
							'#CITY#' => $arVal["CITY_NAME"],
							'#COUNTRY#' => $arVal["COUNTRY_NAME"]
						]);
					}
				}
				
				$propsValues .= $propsValue . PHP_EOL;
			}
			
			$order = \Bitrix\Sale\order::load($orderId);
			if($order->getId() > 0)
			{
				$shipmentCollection = $order->getShipmentCollection();
				$shipment = reset(end($shipmentCollection));
				if($shipment->getDeliveryName())
					$propsValues .= GetMessage('DELIVERY_FIELD', ['#DELIVERY#' => $shipment->getDeliveryName()]) . PHP_EOL;
				
				$paymentCollection = $order->getPaymentCollection();
				$payment = reset(end($paymentCollection));
				if($payment->getPaymentSystemName())
					$propsValues .= GetMessage('PAYSYSTEM_FIELD', ['#PAYSYS#' => $payment->getPaymentSystemName()]);
			}
			
			if(!empty($propsValues))
				return $propsValues;
		}
		else return GetMessage('ERROR_FIELDS');
	}
	
	protected function _getOrderId()
	{
		global $USER;
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $USER->GetID());
		$orderId = (int)$userFields['UF_ITANDYR_LAST_ORD']['VALUE'] > 0 ? (int)$userFields['UF_ITANDYR_LAST_ORD']['VALUE'] : false;
		
		return $orderId;
	}
	
	protected function _getPropValue($propId = false)
	{
		$orderId = $this->_getOrderId();
		if($orderId && $propId)
		{
			$order = \Bitrix\Sale\order::load($orderId);
			$propertyCollection = $order->getPropertyCollection();
			$nameProp = $propertyCollection->getItemByOrderPropertyId($propId);
			return !empty($nameProp->getValue()) ? $nameProp->getValue() : false;
		}
		else
			return false;
	}

	public function __construct()
    {
		global $botOptions;
        $this->opt = $botOptions;
		
		//add user fields if need
		Utils::addOrderUserField();
    }
	
	public function isOrderProcess()
	{
		global $USER;
		$uid = $USER->GetID();
		if((int)$uid == 0) return false;
		
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $uid);
		if($userFields['UF_ITANDYR_IS_ORDER']['VALUE'] == 'Y')
			return true;
		
		return false;
	}

    public function getOrderStatusList()
    {
    	$dbStatus = \CSaleStatus::GetList(['SORT' => 'ASC'], ['LID' => 'ru']);
		while($obStatus = $dbStatus->GetNext())
		{
			$keyboard[] = [['text' => $obStatus['NAME'], 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('getOrderList'), 'id' => $obStatus['ID']])]];
		}
		$text = GetMessage('CHOOSE_STATUS');
		return new Msg($text, $keyboard);
    }
	
	public function getOrderList($status = 'N')
	{
		global $USER;
		$uid = $USER->GetID();

		$status = \CSaleStatus::GetByID($status);
		$text = GetMessage('ORDER_LIST_TEXT', ['#STATUS_NAME#' => $status['NAME']]);
		$dbOrders = \CSaleOrder::GetList(["DATE_INSERT" => "DESC"], ['USER_ID' => $uid, 'STATUS_ID' => $status['ID']]);
		
		while($obOrders = $dbOrders->GetNext())
		{
			$priceStr = \CCurrencyLang::CurrencyFormat(
				$obOrders['PRICE'], 
				$obOrders['CURRENCY'],
				TRUE
			);
			$keyboard[] = [['text' => GetMessage('ORDER_ITEM', ['#ID#'=>$obOrders['ID'], '#SUM#' => $priceStr]), 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('getOrder'), 'id' => $obOrders['ID']])]];
		}

		$keyboard[] = [['text' => $this->opt->getTitle('BACK_BUTTON'), 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('getOrderStatusList')])]];

		return new Msg($text, $keyboard);
	}

	public function getOrder($orderId = 0)
	{
		global $USER;
		$uid = $USER->GetID();
		//$fUser = \CSaleBasket::GetBasketUserID();
		$siteId = $this->opt->getOption('main', 'bot_site_id');

		$order = \Bitrix\Sale\Order::load($orderId);

		if($order)
		{
			$status = \CSaleStatus::GetByID($order->getField("STATUS_ID"));
			$priceStr = \CCurrencyLang::CurrencyFormat(
				$order->getPrice(), 
				$order->getCurrency(),
				TRUE
			);
			$deliveryPriceStr = \CCurrencyLang::CurrencyFormat(
				$order->getDeliveryPrice(), 
				$order->getCurrency(),
				TRUE
			);

			$arDeliv = \Bitrix\Sale\Delivery\Services\Manager::getById($order->getField("DELIVERY_ID"));
			$arPaySys = \CSalePaySystem::GetByID($order->getField("PAY_SYSTEM_ID"), $order->getPersonTypeId());

			$items = '';
			$dbBasketItems = \CSaleBasket::GetList(array("ID" => "DESC"), array(/*"FUSER_ID" => $fUser,*/ "LID" => $siteId, "ORDER_ID" => $orderId), false, false, array());

			while ($arItem = $dbBasketItems->Fetch())
			{
				$priceItem = \CCurrencyLang::CurrencyFormat(
					$arItem['PRICE'] * $arItem['QUANTITY'], 
					$arItem['CURRENCY'],
					TRUE
				);
				$items .= $arItem['NAME'] . ' - ' . $priceItem . PHP_EOL;
			}

			$text = GetMessage('FULL_ORDER', [
				'#ID#' => $order->getId(),
				'#DATE_INSERT#' => $order->getField("DATE_INSERT"),
				'#STATUS#' => $status['NAME'],
				'#SUM#' => $priceStr,
				'#DELIVERY_SUM#' => $deliveryPriceStr,
				'#DELIVERY#' => $arDeliv['NAME'],
				'#PAYSYSTEM#' => $arPaySys["NAME"],
				'#PAY_STATUS#' => !$order->isPaid() ? GetMessage('PAY_FALSE') : GetMessage('PAY_TRUE'),
				'#ITEMS#' => PHP_EOL . $items
			]);

			if(!$order->isPaid())
				$keyboard[] = [['text' => GetMessage('PAY_ORDER'), 'url' => CatalogUtils::getPayLink($orderId)]];
		}
		
		$keyboard[] = [['text' => $this->opt->getTitle('BACK_BUTTON'), 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('getOrderList'), 'id' => 'N'])]];

		return new Msg($text, $keyboard);
	}

	public function createOrder()
	{
		/*READ FIELDS*/
		global $USER;
		$uid = $USER->GetID();
		$fUser = \CSaleBasket::GetBasketUserID();
		$siteId = $this->opt->getOption('main', 'bot_site_id');
		$currency =  $this->opt->getOption('catalog', 'bot_catalog_currency');
		$person = $this->opt->getOption('order', 'bot_order_person_type');
		
		$orderId = false;
		//check for process order
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $uid);
		if($userFields['UF_ITANDYR_IS_ORDER']['VALUE'] !== 'Y')
		{
			/*CREATE ORDER*/
			$order = \Bitrix\Sale\order::create($siteId, $uid);
			$order->setPersonTypeId($person);

			/*GET PRODUCTS*/
			$basketClass = new \CSaleBasket();
			$dbBasketItems = $basketClass->GetList(array("ID" => "DESC"), array("FUSER_ID" => $fUser, "LID" => $siteId, "ORDER_ID" => "NULL"), false, false, array());
			$products = [];
			
			while ($arItem = $dbBasketItems->Fetch())
			{
				if($currency !== $arItem['CURRENCY'])
					$arItem['PRICE'] = \CCurrencyRates::ConvertCurrency($arItem['PRICE'], $arItem['CURRENCY'], $currency);
				
				$products[] = [
					'PRODUCT_ID' => $arItem['PRODUCT_ID'],
					'NAME' => $arItem['NAME'],
					'PRICE' => $arItem['PRICE'],
					'CURRENCY' => $currency,
					'QUANTITY' => $arItem['QUANTITY'],
				];
			}

			/*SET BASKET*/
			$basket = \Bitrix\Sale\Basket::create($siteId);
			foreach ($products as $product)
			{
				$item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
				unset($product["PRODUCT_ID"]);
				$item->setFields($product);
			}
			$order->setBasket($basket);
			
			/*SAVE ORDER*/
			$order->doFinalAction(true);
			$result = $order->save();
			
			/*ERROR IF ORDER DIDNT CREATE*/
			if(!$result->isSuccess())
			{
				return new Msg(GetMessage('ERROR_ORDER_CREATE'));
			}
			else
			{
				$orderId = $order->GetId();
				/*SET UF FOR THIS ORDER*/
				Utils::setUserField('UF_ITANDYR_LAST_ORD', $orderId);
				Utils::setUserField('UF_ITANDYR_IS_ORDER', 'Y');
			}
		}
		else
		{
			$orderId = ((int)$userFields['UF_ITANDYR_LAST_ORD']['VALUE'] > 0) ? $userFields['UF_ITANDYR_LAST_ORD']['VALUE'] : false;
		}
		
		/*CREATE MESSAGE IF ORDER CREATE SUCCESS*/
		if($orderId > 0)
		{
			//�������� ������ �����
			if(!$this->_isNewUser($uid))
			{
				$text = GetMessage('CHECK_FIELDS') . PHP_EOL;
				
				$dbOrders = \CSaleOrder::GetList(["DATE_INSERT" => "DESC"], ['USER_ID' => $uid, '!ID' => $orderId]);
				if($lastOrd = $dbOrders->GetNext())
					$text .= $this->_getOrderFieldsText($lastOrd['ID']);
				
				$keyboard =[
					[[
						'text' => GetMessage('ERROR'),
							'callback_data' => Utils::getCallBackStr([
								'case' => $this->opt->getCase('createOrder'),
								'type' => 'input'
						])
					]],
					[[
						'text' => GetMessage('APPLY'),
							'callback_data' => Utils::getCallBackStr([
								'case' => $this->opt->getCase('createOrder'),
								'type' => 'autoapply'
						])
					]]
				];
			}
			else
			{
				$text = GetMessage('NEW_USER');
				$keyboard =[[[
						'text' => GetMessage('INPUT_DATA'),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'input'
						])
					]]];
			}
		}
		
		return  new Msg($text, $keyboard);
	}
	
	public function getOrderField($isFirstField = false)
	{
		$fieldId = '';
		
		global $USER;
		$uid = $USER->GetID();
		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $uid);
		
		$optFields = $this->opt->getOption('order', 'bot_order_props');
		if($optFields == NULL) return new Msg('NOTHING_TO_OUT');

		$optFields = explode(',', $optFields);
		
		if($isFirstField)
		{
			$fieldId = reset($optFields);
		}
		elseif(!empty($userFields['UF_ITANDYR_FIELD']['VALUE']) && (int)$userFields['UF_ITANDYR_FIELD']['VALUE'] > 0)
		{	
			$isThisField = false;
			foreach($optFields as $k=>$v)
			{
				if($v == $userFields['UF_ITANDYR_FIELD']['VALUE'])
				{
					$isThisField = true;
					continue;
				}
				
				if($isThisField)
				{
					$fieldId = $v;
					break;
				}
			}
			
			if((end($optFields) == $userFields['UF_ITANDYR_FIELD']['VALUE'] && $fieldId == ''))
				return 'DELIVERY';
		}
		elseif($userFields['UF_ITANDYR_FIELD']['VALUE'] == 'ADD_ADDRESS')
			return 'DELIVERY';
		
		$dbOrdProps = \CSaleOrderProps::GetList(["SORT" => "asc"], [
			"PERSON_TYPE_ID" => $this->opt->getOption('order', 'bot_order_person_type'),
			"LID" => $this->opt->getOption('main', 'bot_site_id'),
			"UTIL" => "N",
			"=ID" => $fieldId
		], false, false, []);
		
		if($obProp = $dbOrdProps->GetNext())
		{
			Utils::setUserField('UF_ITANDYR_FIELD', $fieldId);
			
			switch($obProp['CODE'])
			{
				case $this->opt->getOption('order', 'bot_order_props_phone'):
					return new Msg(GetMessage('PHONE_PROP'), [[['text' => GetMessage('SEND_PHONE'), 'request_contact' => true, 'one_time_keyboard' => true]]]);
				break;
				
				case $this->opt->getOption('order', 'bot_order_props_loc'):
					return new Msg(GetMessage('LOC_PROP'), [[['text' => GetMessage('SEND_LOC'), 'request_location' => true, 'one_time_keyboard' => true]]]);
				break;
				
				case $this->opt->getOption('order', 'bot_order_props_address'):
					if($propValue = $this->_getPropValue($obProp['ID']))
						return new Msg(GetMessage('ADDRESS_VALUE', ['#NAME#' => $obProp['NAME'], '#VAL#' => $propValue, '#BUTTON#' => $this->opt->getTitle('ADD_ADDRESS')]), [
							[['text' => $this->opt->getTitle('ADD_ADDRESS')]],
							[['text' => $propValue]]
						]);
				break;
			}
			
			if($propValue = $this->_getPropValue($obProp['ID']))
			{
				return new Msg(GetMessage('PROP_VALUE', ['#NAME#' => $obProp['NAME'], '#VAL#' => $propValue]), [[['text' => $propValue]]]);
			}
			else return new Msg($obProp['NAME']);
		}
		else
		{
			Utils::setUserField('UF_ITANDYR_FIELD', '');
			return new Msg(GetMessage('ERROR_FIELD'));
		}
	}
		public function getOrderFieldAddAddress()
		{
			Utils::setUserField('UF_ITANDYR_FIELD', 'ADD_ADDRESS');
			return new Msg(GetMessage('ADD_ADDRESS'));
		}
		
		public function getDelivery()
		{
			$text = GetMessage('DELIVERY');
			//get choosen deliveries
			$optFields = $this->opt->getOption('order', 'bot_order_delivery');
			if($optFields == NULL) return new Msg(GetMessage('ERROR_DELIVERY'));
			$optFields = explode(',', $optFields);
			$orderId = $this->_getOrderId();
			if(!$orderId) return new Msg(GetMessage('ERROR_DELIVERY_ORDER'));
			
			//get order obj and locationId
			$order = \Bitrix\Sale\order::load($orderId);
			$propertyCollection = $order->getPropertyCollection();
			$locationId = $propertyCollection->getDeliveryLocation()->getValue();
			
			//get service list
			$services = \Bitrix\Sale\Delivery\Services\Manager::getActive();
			$filteredServices = [];
			$checkResult = true;
			//filter delivery items
			foreach($services as $deliveryId => $serviceItem)
				if((in_array($deliveryId, $optFields) || in_array($serviceItem['PARENT_ID'], $optFields)) && $serviceItem['ACTIVE'] == 'Y')
					if($locationId && $serviceItem['RESTRICTIONS_EXIST'] > 0)
						if(\Bitrix\Sale\Delivery\Restrictions\ByLocation::check($locationId,[],$deliveryId))
							$filteredServices[$deliveryId] = $serviceItem;
					else
						$filteredServices[$deliveryId] = $serviceItem;
			unset($services, $deliveryId, $serviceItem);
			
			//unset parent deliveries
			$hasChildrenDeliveriesKeys = [];
			$keysFiltered = array_keys($filteredServices);
			foreach($filteredServices as $deliveryId => $serviceItem)
				if(in_array($serviceItem['PARENT_ID'], $keysFiltered))
					$hasChildrenDeliveriesKeys[] = $deliveryId;
			$hasChildrenDeliveriesKeys = array_unique($hasChildrenDeliveriesKeys);
			foreach($hasChildrenDeliveriesKeys as $keyToUnset)
				unset($filteredServices[$keyToUnset]);
			unset($hasChildrenDeliveriesKeys, $keysFiltered);
			
			if(count($filteredServices) == 0) return new Msg(GetMessage('ERROR_DELIVERY'));
			
			//set keyboad
			$keyboard = [];
			foreach($filteredServices as $deliveryId => $service)
			{
				$priceStr = false;
				if($service['CONFIG']['MAIN']['PRICE'] > 0)
					$priceStr = \CCurrencyLang::CurrencyFormat(
						$service['CONFIG']['MAIN']['PRICE'], 
						$service['CONFIG']['MAIN']['CURRENCY'],
						TRUE
					);
				
				if($priceStr) //simple delivery with price
					$keyboard[] = [[
						'text' => GetMessage('DELIVERY_ROW',[
							'#DELIVERY_NAME#' => $service['NAME'],
							'#SUM#' => $priceStr
						]),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'delivery',
							'id' => $service['ID']])
					]];
				elseif($service['CONFIG']['MAIN']['PRICE'] === 0) //simple free delivery
					$keyboard[] = [[
						'text' => GetMessage('DELIVERY_ROW_FREE',[
							'#DELIVERY_NAME#' => $service['NAME']
						]),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'delivery',
							'id' => $service['ID']])
					]];
				else //autodeliveries
				{
					$calculateDelivery = $this->_calculateDelivery($order, $deliveryId);
					if(!$calculateDelivery) continue;
					$priceStr = \CCurrencyLang::CurrencyFormat(
						$calculateDelivery, 
						$service['CURRENCY']?:$order->getCurrency(),
						TRUE
					);
					$keyboard[] = [[
						'text' => GetMessage('DELIVERY_ROW',[
							'#DELIVERY_NAME#' => $service['NAME'],
							'#SUM#' => $priceStr
						]),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'delivery',
							'id' => $service['ID']])
					]];
				}
					
			}
			
			return new Msg($text, $keyboard);
		}
			private function _calculateDelivery(\Bitrix\Sale\order $order, $deliveryId)
			{
				$shipmentCollection = $order->getShipmentCollection();
				$shipment = $shipmentCollection->createItem();
				$service = \Bitrix\Sale\Delivery\Services\Manager::getById($deliveryId);
				$shipment->setFields(array(
					'DELIVERY_ID' => $service['ID'],
					'DELIVERY_NAME' => $service['NAME'],
					'CURRENCY' => $order->getCurrency()
				));
				
				$shipmentItemCollection = $shipment->getShipmentItemCollection();
				$basket = $order->getBasket();
				foreach ($basket as $basketItem)
				{
					$item = $shipmentItemCollection->createItem($basketItem);
					$item->setQuantity($basketItem->getQuantity());
				}
				
				$calc = \Bitrix\Sale\Delivery\Services\Manager::calculateDeliveryPrice($shipment, $deliveryId);
				if(count($calc->getErrors()) > 0) return false;
				return $calc->getDeliveryPrice();
			}
		
		public function getPaysystem()
		{
			$text = GetMessage('PAYSYSTEM');
			$paysystem = [];
			$optFields = $this->opt->getOption('order', 'bot_order_paysystem');
			
			if($optFields == NULL) return new Msg('ERROR_PAYSYS');;

			$optFields = explode(',', $optFields);
			$arOrder = array("SORT"=>"ASC", "PSA_NAME"=>"ASC");
			$dbPaysystem = \CSalePaySystem::GetList($arOrder,[
				"ACTIVE" => "Y",
				"PERSON_TYPE_ID" => $this->opt->getOption('order', 'bot_order_person_type'),
				"LID" => $this->opt->getOption('main', 'bot_site_id'),
				"=ID" => $optFields
			]);
			while($obOrdProp = $dbPaysystem->Fetch())
				$paysystem[$obOrdProp['ID']] = $obOrdProp['NAME'];
			
			$keyboard = [];
			foreach($paysystem as $id=>$value)
			{
				$keyboard[] = [['text' => $value, 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('createOrder'), 'type' => 'paysystem', 'id' => $id])]];
			}

			return new Msg($text, $keyboard);
		}
	
	public function setOrderField($value = '')
	{
		if(SITE_CHARSET == 'windows-1251')
		{
			$value = \Bitrix\Main\Text\Encoding::convertEncoding($value, 'UTF-8', 'windows-1251');
		}
		
		global $USER;
		$uid = $USER->GetID();

		$userFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields('USER', $uid);
		$currentField = (int)$userFields['UF_ITANDYR_FIELD']['VALUE'] > 0 ? (int)$userFields['UF_ITANDYR_FIELD']['VALUE'] : false;
		$orderId = (int)$userFields['UF_ITANDYR_LAST_ORD']['VALUE'] > 0 ? (int)$userFields['UF_ITANDYR_LAST_ORD']['VALUE'] : false;
		
		if($currentField && $orderId)
		{
			$order = \Bitrix\Sale\order::load($orderId);
			
			$dbProp = \CSaleOrderProps::GetList([], ['ID' => $currentField]);
			if($obProp = $dbProp->GetNext())
			{
				$propertyCollection = $order->getPropertyCollection();

				if($obProp['IS_EMAIL'] == 'Y')
				{
					$nameProp = $propertyCollection->getUserEmail();
				}
				elseif($obProp['IS_PROFILE_NAME'] == 'Y')
				{
					$nameProp = $propertyCollection->getProfileName();
				}
				elseif($obProp['IS_PAYER'] == 'Y')
				{
					$nameProp = $propertyCollection->getPayerName();
				}
				elseif($obProp['IS_LOCATION'] == 'Y')
				{
					$nameProp = $propertyCollection->getDeliveryLocation();
					$value = $this->_getLocation($value);
					
					if(!empty($value['LOC']))
					{
						if($nameProp !== NULL)
							$nameProp->setValue($value['LOC']);
					}
					
					if(!empty($value['ZIP']))
					{
						$nameProp = $propertyCollection->getDeliveryLocationZip();
						if($nameProp !== NULL)
							$nameProp->setValue($value['ZIP']);
					}
					
					if(!empty($value['ADDRESS']))
					{
						$nameProp = $propertyCollection->getAddress();
						if($nameProp !== NULL)
							$nameProp->setValue($value['ADDRESS']);
					}
					
					$nameProp = NULL;
					
				}
				else
				{
					$nameProp = $propertyCollection->getItemByOrderPropertyId($currentField);
				}
				
				if($nameProp !== NULL)
						$nameProp->setValue($value);
			}
			
			$result = $order->save();
			if (!$result->isSuccess())
			{
				//$result->getErrors();
			}
			else
			{
				
			}
		}
		elseif($userFields['UF_ITANDYR_FIELD']['VALUE'] == 'ADD_ADDRESS')
		{
			$order = \Bitrix\Sale\order::load($orderId);
			$propertyCollection = $order->getPropertyCollection();
			$nameProp = $propertyCollection->getAddress();
			
			if($nameProp !== NULL)
				$nameProp->setValue($nameProp->getValue() . " " . $value);
			
			$result = $order->save();
		}
	}
	
		private function _getLocation($arrLoc = [])
		{
			$lon = $arrLoc['lon'] ? : false;
			$lat = $arrLoc['lat'] ? : false;
			
			if(!$lon || !$lat) return false;
			
			
			$location = simplexml_load_file("https://geocode-maps.yandex.ru/1.x/?geocode={$lon},{$lat}");
			
			if($location !== FALSE)
			{
				
				$arReturn = [];
				$location = Utils::xml2array($location);
				$foundPoints = $location['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'];
				
				if($foundPoints > 0)
				{
					$featureMember = Utils::xml2array($location['GeoObjectCollection']['featureMember'][0]);
					
					if(SITE_CHARSET == 'windows-1251')
						$locationItems = \Bitrix\Main\Text\Encoding::convertEncoding($featureMember['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country'], 'UTF-8', 'windows-1251');
					
					$arReturn['ZIP'] = $featureMember['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['postal_code'];
					$arReturn['ADDRESS'] = $locationItems['AddressLine'];

					
					$city =  $locationItems['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
					$dbLoc = \CSaleLocation::GetList([],['CITY_NAME' => $city], false, false, ['CODE']);
					if($obLoc = $dbLoc->GetNext())
					{
						$arReturn['LOC'] = $obLoc['CODE'];
						return $arReturn;
					}
					
					$region = $locationItems['AdministrativeArea']['AdministrativeAreaName'];
					$dbLoc = \CSaleLocation::GetList([],['REGION_NAME' => $region], false, false, ['CODE']);
					if($obLoc = $dbLoc->GetNext())
					{
						$arReturn['LOC'] = $obLoc['CODE'];
						return $arReturn;
					}
					
					$country = $locationItems['CountryName'];
					$dbLoc = \CSaleLocation::GetList([],['COUNTRY_NAME' => $country], false, false, ['CODE']);
					if($obLoc = $dbLoc->GetNext())
					{
						$arReturn['LOC'] = $obLoc['CODE'];
						return $arReturn;
					}
					
				}
				else
				{
					return false;
				}
			}
		}
	
		public function setOrderDelivery($value = false)
		{
			$orderId = $this->_getOrderId();
			if($orderId && $value)
			{
				$order = \Bitrix\Sale\order::load($orderId);
				$shipmentCollection = $order->getShipmentCollection();
				if(count($shipmentCollection) >= 2)
				{
					$shipment = reset(end($shipmentCollection));
					$shipment->delete();
				}
				
				$shipment = $shipmentCollection->createItem();
				
				$service = \Bitrix\Sale\Delivery\Services\Manager::getById($value);
				$shipment->setFields(array(
					'DELIVERY_ID' => $service['ID'],
					'DELIVERY_NAME' => $service['NAME'],
					'CURRENCY' => $order->getCurrency()
				));		

				$shipmentItemCollection = $shipment->getShipmentItemCollection();
				$basket = $order->getBasket();
				foreach ($basket as $basketItem)
				{
					$item = $shipmentItemCollection->createItem($basketItem);
					$item->setQuantity($basketItem->getQuantity());
				}
				
				$shipmentCollection->calculateDelivery();
				
				$result = $order->save();
			}
		}
		
		public function setOrderPaysystem($value = false)
		{
			$orderId = $this->_getOrderId();
			if($orderId && $value)
			{
				$order = \Bitrix\Sale\order::load($orderId);
				$paymentCollection = $order->getPaymentCollection();
				if(count($paymentCollection) >= 1)
				{
					$payment = reset(end($paymentCollection));
					$payment->delete();
				}
				
				$payment = $paymentCollection->createItem();
				$paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($value);

				$payment->setFields(array(
					'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
					'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
					'SUM' => $order->getPrice()
				));
				
				$result = $order->save();
			}
		}
	
	public function checkOrder()
	{
		$orderId = $this->_getOrderId();
		if($orderId)
		{
			$text = GetMessage('CHECK_FIELDS') . PHP_EOL;
			$text .= $this->_getOrderFieldsText($orderId);
			
			$keyboard =[
				[[
					'text' => GetMessage('ERROR'),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'input'
					])
				]],
				[[
					'text' => GetMessage('APPLY'),
						'callback_data' => Utils::getCallBackStr([
							'case' => $this->opt->getCase('createOrder'),
							'type' => 'apply'
					])
				]]
			];
			
			return new Msg($text, $keyboard);
		}
	}
	
	public function saveOrder()
	{
		global $USER;
		
		$orderId = $this->_getOrderId();
		if($orderId)
			$order = \Bitrix\Sale\order::load($orderId);
		
		$keyboard = [[
			['text' => GetMessage('ORDER_LIST'), 'callback_data' => Utils::getCallBackStr(['case' => $this->opt->getCase('getOrderList'), 'id' => 'N'])],
			['text' => GetMessage('PAY_ORDER'), 'url' => CatalogUtils::getPayLink($order->GetId())]
		]];
		
		\CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());
		Utils::setUserField('UF_ITANDYR_LAST_ORD', '');
		Utils::setUserField('UF_ITANDYR_IS_ORDER', 'N');
				
		return new Msg(GetMessage('SUCCESS_ORDER', ['#ID#' => $order->GetId()]), $keyboard);
	}
	
	public function autoSaveOrder()
	{
		$orderId = $this->_getOrderId();
		$dbOrders = \CSaleOrder::GetList(["DATE_INSERT" => "DESC"], ['!ID' => $orderId]);
		
		if($orderId && $obLastOrd = $dbOrders->GetNext())
		{
			$order = \Bitrix\Sale\order::load($orderId);
			$propertyCollection = $order->getPropertyCollection();
			
			$lastOrd = \Bitrix\Sale\order::load($obLastOrd['ID']);
			$lastPropertyCollection = $lastOrd->getPropertyCollection();
			
			
			$dbProps = \CSaleOrderPropsValue::GetOrderProps($obLastOrd['ID']); //read props in last order
			
			while($arProps = $dbProps->Fetch())
			{
				$currentProp = $lastPropertyCollection->getItemByOrderPropertyId($arProps['ORDER_PROPS_ID']);
				if($arProps['IS_EMAIL'] == 'Y')
					$nameProp = $propertyCollection->getUserEmail();
				elseif($arProps['IS_PROFILE_NAME'] == 'Y')
					$nameProp = $propertyCollection->getProfileName();
				elseif($arProps['IS_PAYER'] == 'Y')
					$nameProp = $propertyCollection->getPayerName();
				else
					$nameProp = $propertyCollection->getItemByOrderPropertyId($arProps['ORDER_PROPS_ID']);
				
				$nameProp->setValue($currentProp->getValue());
			}
			
			$order->save();
			$this->setOrderDelivery($obLastOrd['DELIVERY_ID']);
			$this->setOrderPaysystem($obLastOrd['PAY_SYSTEM_ID']);
			
			return $this->saveOrder();
		}
	}
}